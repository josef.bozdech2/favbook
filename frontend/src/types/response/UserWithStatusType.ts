export interface UserWithStatus extends User {
  status: number;
}

export type FindSearchedUsersResponse = {
  userList: UserWithRolesAndRelation[] | null;
  message: string;
};

export interface UserWithRoles extends User {
  roles: Role[];
}

export type Role = {
  role: string;
};

export interface UserWithStatusAndRoles extends UserWithStatus, UserWithRoles {}

export type User = {
  id: string;
  email: string;
  name: string;
  surname: string;
};

export interface UserWithPassword extends User {
  password: string;
}

export interface Status {
  id: string;
  type: number;
  typeString: string;
}

export interface Relation {
  id: string;
  userId1: string;
  userId2: string;
  status: Status;
}

export interface UserWithRolesAndRelation extends UserWithRoles {
  status: number | undefined;
}
