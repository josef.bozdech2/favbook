import { User } from './UserWithStatusType';

export interface Message {
  id: string;
  user: User;
  content: string;
  timestamp: Date;
  chatId: string;
}
