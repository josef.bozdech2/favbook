export interface SignUpParams {
  email: string;
  name: string;
  surname: string;
  password: string;
}

export interface LoginParams {
  email: string;
  password: string;
}
