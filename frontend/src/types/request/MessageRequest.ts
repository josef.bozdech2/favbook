export interface LoadMoreMessagesRequest {
  friendId: string;
  offset?: number;
}
