export interface NameObject {
  name: ContainsObject;
}

export interface SurnameObject {
  surname: ContainsObject;
}

export interface ContainsObject {
  contains: string;
}

export type Object = NameObject | SurnameObject;
