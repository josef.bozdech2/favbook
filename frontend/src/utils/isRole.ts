import { Role } from "types/response/UserWithStatusType";

export enum existingRoles {
  user = 'USER',
  admin = 'ADMIN',
  god = 'GOD',
}

export const isRole = (roles: Role[] | null, requestedRole: string) => {
  if (roles === null || roles === undefined) return false;
  const filteredRoles = roles.filter(role => {
    return role.role === requestedRole;
  });
  return filteredRoles.length > 0;
};
