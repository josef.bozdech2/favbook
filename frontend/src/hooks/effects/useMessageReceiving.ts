import { useEffect } from 'react';
import { Socket } from 'socket.io-client';
import { Message } from 'types/response/MessageResponse';
import { EVENT_MESSAGES } from 'utils/socketMessageEnum';
type MessageWrapper = {
  message: Message;
  mergeNewMessageWithExisting: (message: Message) => void;
};
const useMessageReceiving = (
  socket: Socket | null,
  setMessages,
  selectedChat,
  userId,
) => {
  const checkDupicates = (prevMessages: Message[], newMessage: Message) => {
    const isMessageExist = prevMessages.map(message => {
      if (message.id === newMessage.id) return true;
      return false;
    });
    return isMessageExist.includes(true);
  };
  return useEffect(() => {
    socket?.on(EVENT_MESSAGES.privateMessage, ({ message }: MessageWrapper) => {
      if (selectedChat === message.user.id || message.user.id === userId) {
        setMessages(prevMessages => {
          if (checkDupicates(prevMessages, message)) return prevMessages;
          return [...prevMessages, message];
        });
      }
    });
  }, [selectedChat, setMessages, socket, userId]);
};

export default useMessageReceiving;
