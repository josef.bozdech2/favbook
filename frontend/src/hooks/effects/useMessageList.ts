import { MessageListWrapper } from 'app/pages/PostsPage';
import { useEffect } from 'react';
import { Socket } from 'socket.io-client';
import { EVENT_MESSAGES } from 'utils/socketMessageEnum';

const useMessageList = (
  setMessages,
  selectedChat: string,
  socket: Socket | null,
) => {
  return useEffect(() => {
    setMessages([]);
    socket?.emit(EVENT_MESSAGES.loadMoreMessages, { friendId: selectedChat });
    socket?.on(
      EVENT_MESSAGES.getMoreMessages,
      ({ messageList }: MessageListWrapper) => {
        if (messageList?.length) {
          setMessages(messageList);
        }
      },
    );
  }, [selectedChat, setMessages]);
};

export default useMessageList;
