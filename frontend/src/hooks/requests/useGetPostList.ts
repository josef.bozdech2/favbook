import { AxiosResponse } from 'axios';
import { useQuery } from 'react-query';
import axios from 'axios';

interface PostListParams {}

export const useGetPostList = (params: PostListParams) => {
  return useQuery(
    'postList',
    async () => {
      const dtoOut: AxiosResponse | void = await axios.get('/posts', {
        params,
      });
      return dtoOut;
    },
    { refetchInterval: 5000 },
  );
};
