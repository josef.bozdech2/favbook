import { AxiosResponse } from 'axios';
import { Dispatch, SetStateAction } from 'react';
import { useMutation } from 'react-query';
import axios from 'axios';
import { SignUpParams } from 'types/request/SignUpRequest';

export const useSignUpMutation = (
  setOpenAlert: Dispatch<SetStateAction<boolean>>,
  setAlertMessage: Dispatch<SetStateAction<string>>,
  callback?: (params: AxiosResponse<any, any> | void) => Promise<unknown>,
) => {
  return useMutation(async (_signUpParams: SignUpParams) => {
    const dtoOut: AxiosResponse | void = await axios
      .post('/signup', _signUpParams)
      .catch(error => {
        setAlertMessage(error.response.data.message);
        setOpenAlert(true);
      });
    if (dtoOut && dtoOut?.data?.token) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + dtoOut?.data?.token;
    }
    callback && callback(dtoOut);
  });
};
