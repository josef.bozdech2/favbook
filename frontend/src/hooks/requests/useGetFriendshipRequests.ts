import { AxiosResponse } from 'axios';
import { useQuery } from 'react-query';
import axios from 'axios';

interface FriendshipRequestParams {
  id: string | null;
}

export const useGetFriendshipRequests = (params: FriendshipRequestParams) => {
  return useQuery(
    'friendshipRequestList',
    async () => {
      const dtoOut: AxiosResponse | void = await axios.get('/users/relations', {
        params,
      });
      return dtoOut;
    },
    { refetchInterval: 5000 },
  );
};
