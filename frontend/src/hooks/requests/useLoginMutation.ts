import { AxiosResponse } from 'axios';
import { Dispatch, SetStateAction } from 'react';
import { useMutation } from 'react-query';
import axios from 'axios';
import { LoginParams } from 'types/request/SignUpRequest';

export const useLoginMutation = (
  setOpenAlert: Dispatch<SetStateAction<boolean>>,
  setAlertMessage: Dispatch<SetStateAction<string>>,
  callback?: (params: AxiosResponse<any, any> | void) => Promise<unknown>,
) => {
  return useMutation(async (_loginParams: LoginParams) => {
    const dtoOut: AxiosResponse | void = await axios
      .post('/login', _loginParams)
      .catch(error => {
        setAlertMessage(error?.response?.data?.message);
        setOpenAlert(true);
      });
    if (dtoOut && dtoOut?.data?.token) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + dtoOut?.data?.token;
    }
    callback && callback(dtoOut);
  });
};
