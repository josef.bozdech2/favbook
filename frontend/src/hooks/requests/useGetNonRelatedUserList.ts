import { AxiosResponse } from 'axios';
import { useMutation } from 'react-query';
import axios from 'axios';
import { RequestIdEnum } from './requestIdEnum';
import { FindSearchedUsersResponse } from 'types/response/UserWithStatusType';

interface GetNonRelatedUserListParams {
  id: string | null;
}

export const useGetNonRelatedUserList = (
  params: GetNonRelatedUserListParams,
) => {
  return useMutation(
    RequestIdEnum.getNonRelatedUserList,
    async (
      searchString: string,
    ): Promise<AxiosResponse<Partial<FindSearchedUsersResponse>> | void> => {
      return axios.get('/users/search', {
        params: { ...params, searchString },
      });
    },
  );
};
