import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import LoginAlert from 'app/components/LoginAlert';
import { AxiosResponse } from 'axios';
import { AuthContextType, useAuthContext } from 'contexts/authContext';
import { useSignUpMutation } from 'hooks/requests/useSignUpMutation';
import * as React from 'react';
import { useCookies } from 'react-cookie';
import { Link } from 'react-router-dom';
import { io, Socket } from 'socket.io-client';
import { UserWithRoles } from 'types/response/UserWithStatusType';

type UseSignUpMutationType = {
  cookie: string;
  data: UserWithRoles;
  token: string;
};
function Copyright(props: any) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {'Copyright © '}
      {'Hladomor s.r.o.'} {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();

export const SignUpPage = () => {
  const [isEmailExist, setIsEmailExist] = React.useState(false);
  const [passwordRepeatDone, setPasswordRepeatDone] = React.useState(false);
  const [cookies, setCookie] = useCookies(['name']);
  const { setAuthState } = useAuthContext()!;
  const [openAlert, setOpenAlert] = React.useState(false);
  const [alertMessage, setAlertMessage] = React.useState('');

  const url: string | undefined = String(process.env.REACT_APP_SERVER_URL);

  const signUpMutation = useSignUpMutation(
    setOpenAlert,
    setAlertMessage,
    async (dtoOut: AxiosResponse<UseSignUpMutationType> | void) => {
      if (dtoOut && dtoOut.status === 200) {
        let newState: Partial<AuthContextType> = {
          isAuth: true,
          userName: `${dtoOut.data.data.name} ${dtoOut.data.data.surname}`,
          userId: dtoOut.data.data.id,
          roles: dtoOut.data.data.roles,
          jwt: dtoOut.data.token,
        };
        const newSocket = io(url, {
          auth: { token: dtoOut.data.token },
          upgrade: false,
        });
        createSocketConnection(newSocket, dtoOut.data.token);
        newState = { ...newState, socket: newSocket };
        await setAuthState(newState);
      }
    },
  );
  const createSocketConnection = async (newSocket: Socket, token: string) => {
    await newSocket.emit('userLogin');
  };
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    if (data.get('password') !== data.get('passwordRepeat')) {
      setPasswordRepeatDone(true);
      return;
    } else {
      setPasswordRepeatDone(false);
    }
    const signUpParams = {
      email: String(data.get('email')),
      password: String(data.get('password')),
      name: String(data.get('name')),
      surname: String(data.get('surname')),
    };
    if (
      signUpParams.email === '' ||
      signUpParams.name === '' ||
      signUpParams.surname === '' ||
      signUpParams.password === ''
    )
      alert('Missing params in form!');
    signUpMutation.mutate(signUpParams);
  };
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            {openAlert ? (
              <LoginAlert setOpenAlert={setOpenAlert} message={alertMessage} />
            ) : null}
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="name"
                  required
                  fullWidth
                  id="name"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="surname"
                  label="Last Name"
                  name="surname"
                  autoComplete="family-name"
                />
              </Grid>
              {!isEmailExist ? (
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                </Grid>
              ) : (
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    error
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    helperText="Email already exists."
                  />
                </Grid>
              )}
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                />
              </Grid>
              <Grid item xs={12}>
                {!passwordRepeatDone ? (
                  <TextField
                    required
                    fullWidth
                    name="passwordRepeat"
                    label="Repeat Password"
                    type="password"
                    id="passwordRepeat"
                    autoComplete="new-password"
                  />
                ) : (
                  <TextField
                    required
                    fullWidth
                    error
                    name="passwordRepeat"
                    label="Repeat Password"
                    type="password"
                    id="passwordRepeat"
                    autoComplete="new-password"
                    helperText="Passwords do not match."
                  />
                )}
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link to="/">Already have an account? Sign in</Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
};
