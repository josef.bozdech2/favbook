import { useFriendlistContext } from 'contexts/friendListContext';
import { useEffect, useState } from 'react';
import { User } from 'types/response/UserWithStatusType';
import { EVENT_MESSAGES } from 'utils/socketMessageEnum';

export const useFriendList = socket => {
  const { friendList, setFriendList } = useFriendlistContext()!;
  useEffect(() => {
    socket?.on(EVENT_MESSAGES.currentlyOnlineUsers, ({ users }) => {
      const onlineUsers: User[] = users;
      setFriendList({ friendList: onlineUsers });
    });
    socket?.on(EVENT_MESSAGES.userConnected, ({ user }) => {
      const newUser: User = user;
      const newFriendList: User[] = friendList;
      const friendListIds: string[] = friendList?.map(friend => {
        return friend.id;
      });
      if (!friendListIds.includes(newUser.id)) {
        newFriendList.push(newUser);
        setFriendList({ friendList: newFriendList });
      }
    });
    socket?.on(EVENT_MESSAGES.userDisconnected, ({ user }) => {
      //remove user from array
      const newFriendList = friendList.filter(currentUser => {
        if (currentUser.id !== user.id) {
          return true;
        }
        return false;
      });
      setFriendList({ friendList: newFriendList });
    });
    socket?.on(EVENT_MESSAGES.acceptedFriendship, ({ users }) => {
      const onlineUsers: User[] = users;
      setFriendList({ friendList: onlineUsers });
    });
    socket?.on(EVENT_MESSAGES.removedFriend, ({ users }) => {
      const onlineUsers: User[] = users;
      setFriendList({ friendList: onlineUsers });
    });
    socket?.on(EVENT_MESSAGES.blockedFriend, ({ users }) => {
      const onlineUsers: User[] = users;
      setFriendList({ friendList: onlineUsers });
    });
  }, [socket, friendList, setFriendList]);
};
