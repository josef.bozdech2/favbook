import FaceTwoToneIcon from '@mui/icons-material/FaceTwoTone';
import {
  Grid,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader
} from '@mui/material';
import React from 'react';
import { User } from 'types/response/UserWithStatusType';

const openUserChat = (
  userId: string,
  setSelectedChat: (userId: string) => {},
) => {
  setSelectedChat(userId);
};

const Friendlist = ({ friendList, socket, selectedChat, setSelectedChat }) => {
  return (
    <>
      <Grid container item xs={2}>
        <List subheader={<ListSubheader>Online users</ListSubheader>}>
          {renderUserList(friendList, selectedChat, setSelectedChat)}
        </List>
      </Grid>
    </>
  );
};

const renderSingleUser = (
  user: User,
  selectedChat: string,
  setSelectedChat: () => {},
) => {
  const parsedName = `${user.name} ${user.surname}`;
  return (
    <ListItemButton
      sx={{ backgroundColor: user.id === selectedChat ? '#dedede' : 'white' }}
      key={user.id}
      onClick={() => openUserChat(user.id, setSelectedChat)}
    >
      <ListItemIcon>
        <FaceTwoToneIcon />
      </ListItemIcon>
      <ListItemText primary={parsedName} />
    </ListItemButton>
  );
};

const renderUserList = (
  users: User[],
  selectedChat: string,
  setSelectedChat: () => {},
) => {
  return users.map(item =>
    renderSingleUser(item, selectedChat, setSelectedChat),
  );
};

export default Friendlist;
