import FaceIcon from '@mui/icons-material/Face';
import {
  Avatar,
  Dialog, List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Slide
} from '@mui/material';
import { TransitionProps } from '@mui/material/transitions';
import React from 'react';
import { User } from 'types/response/UserWithStatusType';

const PostUserLikesDialogue = ({ users, dialogueOpen, setDialogueOpen }) => {
  const handleClose = () => {
    setDialogueOpen(false);
  };
  const renderUsers = (users: User[] | null | undefined) => {
    return users && users?.map(item => renderSingleUser(item));
  };
  const renderSingleUser = (user: User) => {
    return (
      <ListItem key={user.id}>
        <ListItemAvatar>
          <Avatar>
            <FaceIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={`${user.name} ${user.surname}`} />
      </ListItem>
    );
  };

  return (
    <>
      <Dialog onClose={handleClose} open={dialogueOpen}>
        <List sx={{ pt: 0 }}>{renderUsers(users)}</List>
      </Dialog>
    </>
  );
};

export default PostUserLikesDialogue;

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});
