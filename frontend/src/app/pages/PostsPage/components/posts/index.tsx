import { Grid } from '@mui/material';
import { useAuthContext } from 'contexts/authContext';
import { useGetPostList } from 'hooks/requests/useGetPostList';
import { useEffect, useState } from 'react';
import { PostWithUser } from 'types/response/PostType';
import { User } from 'types/response/UserWithStatusType';
import PostUserLikesDialogue from '../postUserLikesDialogue';
import { PostPublishField } from './PostPublishField';
import { PostsCard } from './PostsCard';

export const Posts = () => {
  const { status, data } = useGetPostList({});
  const [dialogueOpen, setDialogueOpen] = useState<boolean>(false);
  const { userId } = useAuthContext()!;
  const [openedPostLikes, setOpenedPostLikes] = useState<User[]>([]);
  const [posts, setPosts] = useState<PostWithUser[]>([]);
  useEffect(() => {
    setPosts(prevPosts => {
      return filterExistingPosts(prevPosts, data && data?.data?.postList);
    });
  }, [data && data?.data?.postList]);
  function filterExistingPosts(
    prevPosts: PostWithUser[],
    newPosts: PostWithUser[],
  ): PostWithUser[] {
    const allPosts: PostWithUser[] = newPosts;
    const postIndexes: string[] = allPosts?.map(post => {
      return post.id;
    });
    prevPosts?.forEach(post => {
      if (!postIndexes.includes(post.id)) allPosts.push(post);
    });
    return allPosts;
  }
  function renderContent() {
    if (status === 'loading') {
      return <span>Loading...</span>;
    }
    if (status === 'success') {
      const postList: PostWithUser[] = posts;
      return postList?.map(post => (
        <PostsCard
          key={post.id}
          post={post}
          userId={userId || ''}
          dialogueOpen={dialogueOpen}
          setDialogueOpen={setDialogueOpen}
          setOpenedPostLikes={setOpenedPostLikes}
        ></PostsCard>
      ));
    }
    return null;
  }
  return (
    <>
      <Grid container item xs={7}>
        <PostPublishField />
        <Grid
          item
          xs={12}
          sx={{
            paddingTop: '10px',
          }}
        >
          {renderContent()}
        </Grid>
      </Grid>
      <PostUserLikesDialogue
        users={openedPostLikes}
        dialogueOpen={dialogueOpen}
        setDialogueOpen={setDialogueOpen}
      />
    </>
  );
};
