import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbUpAltOutlinedIcon from '@mui/icons-material/ThumbUpAltOutlined';
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
} from '@mui/material';
import { useAuthContext } from 'contexts/authContext';
import React, { Dispatch, SetStateAction } from 'react';
import axios from 'axios';
import { User } from 'types/response/UserWithStatusType';
import { PostWithUser } from 'types/response/PostType';

type Props = {
  post: PostWithUser;
  userId: string;
  dialogueOpen: boolean;
  setDialogueOpen: Dispatch<SetStateAction<boolean>>;
  setOpenedPostLikes: Dispatch<SetStateAction<User[]>>;
};
export const PostsCard = ({
  post,
  userId,
  setOpenedPostLikes,
  setDialogueOpen,
}: Props) => {
  function changePostLike(postId: string, setAsLike: boolean) {
    axios.post('/posts/likes', {
      postId,
      setAsLike,
    });
  }
  function isLikedByUser(post: PostWithUser, userId: string) {
    const filteredLikes = post.likes.filter(user => {
      return user.id === userId;
    });
    return filteredLikes.length > 0;
  }
  function isOwnPost(post: PostWithUser, userId: string) {
    return post.userId === userId;
  }

  return (
    <>
      <Card sx={{ marginBottom: '16px' }}>
        <CardHeader subheader={`${post.user.name} ${post.user.surname}`} />
        <CardContent>{post.content}</CardContent>
        <CardActions disableSpacing>
          {!isOwnPost(post, userId) ? (
            isLikedByUser(post, userId) ? (
              <IconButton
                aria-label="add to favorites"
                onClick={() => changePostLike(post.id, false)}
              >
                <ThumbUpIcon />
              </IconButton>
            ) : (
              <IconButton
                aria-label="add to favorites"
                onClick={() => changePostLike(post.id, true)}
              >
                <ThumbUpAltOutlinedIcon />
              </IconButton>
            )
          ) : null}
          <CardActionArea
            onClick={() => {
              setOpenedPostLikes(post.likes);
              setDialogueOpen(true);
            }}
          >
            {post.likesCount}
          </CardActionArea>
        </CardActions>
      </Card>
    </>
  );
};
