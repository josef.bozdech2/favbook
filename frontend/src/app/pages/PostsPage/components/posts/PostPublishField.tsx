import {
  Button,
  Divider,
  FormControlLabel,
  Grid,
  Switch,
  TextField,
} from '@mui/material';
import { useAuthContext } from 'contexts/authContext';
import React, { useState } from 'react';
import axios from 'axios';
import { existingRoles, isRole } from 'utils/isRole';

export const PostPublishField = () => {
  const [postFieldValue, setPostFieldValue] = useState<string>('');
  const [isAnnouncement, setIsAnnouncement] = useState<boolean>(false);
  const [isPostFieldEmpy, setIsPostFieldEmpty] = useState<boolean>(false);
  const { jwt, roles } = useAuthContext()!;

  const handleAnnouncementChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setIsAnnouncement(event.target.checked);
  };

  const handlePostFieldChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setPostFieldValue(event.target.value);
  };
  const publishPost = async () => {
    const post = {
      userId: jwt,
      content: postFieldValue,
      isAnnouncement,
    };
    if (post.content === '') {
      setIsPostFieldEmpty(true);
    } else {
      setIsPostFieldEmpty(false);
      await axios.post('/posts', { post });
    }
    setPostFieldValue('');
  };
  return (
    <>
      <Grid container item xs={12} sx={{ display: 'flex' }} spacing={1}>
        <Grid item xs={12}>
          {!isPostFieldEmpy ? (
            <TextField
              id="outlined-multiline-flexible"
              label="Write a post"
              multiline
              rows={4}
              value={postFieldValue}
              onChange={handlePostFieldChange}
              sx={{
                width: '100%',
              }}
              inputProps={{ maxLength: 255 }}
            />
          ) : (
            <TextField
              id="outlined-multiline-flexible"
              label="Write a post"
              error
              helperText="Post cannot be empty"
              multiline
              rows={4}
              value={postFieldValue}
              onChange={handlePostFieldChange}
              sx={{
                width: '100%',
              }}
              inputProps={{ maxLength: 255 }}
            />
          )}
        </Grid>
        <Grid item xs={2} sx={{ justifyContent: 'flex-end' }}>
          <Button onClick={publishPost} variant="contained">
            Publish
          </Button>
        </Grid>
        {isRole(roles, existingRoles.admin) ? (
          <Grid item xs={4} sx={{ justifyContent: 'flex-end' }}>
            <FormControlLabel
              control={
                <Switch
                  checked={isAnnouncement}
                  onChange={handleAnnouncementChange}
                />
              }
              label="Set as announcement"
            />
          </Grid>
        ) : null}
      </Grid>
      <Grid item xs={12}>
        <Divider
          sx={{
            paddingTop: '10px',
          }}
        />
      </Grid>
    </>
  );
};
