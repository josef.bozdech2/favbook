import { Fab } from '@mui/material';
import { styled } from '@mui/system';
import React, { useState } from 'react';
import SendIcon from '@mui/icons-material/Send';

export const MessageEditor = props => {
  const [value, setValue] = useState(props.value);
  const [isDisabled, setIsDisabled] = useState(false);

  async function _handleSave() {
    if (!value || !value.length) return;
    if (isDisabled) return;
    setIsDisabled(true);
    try {
      await props.handleSave(value);
      setValue('');
    } catch (error) {
      alert('Message could not be send');
    }
    setIsDisabled(false);
  }

  return (
    <Wrapper>
      <TextAreaWrapper>
        <textarea
          value={value}
          rows={1}
          placeholder="Start typing..."
          onChange={e => setValue(e?.currentTarget?.value)}
        ></textarea>
      </TextAreaWrapper>

      <SendIconWrapper onClick={_handleSave}>
        <SendIcon />
      </SendIconWrapper>
    </Wrapper>
  );
};

export default MessageEditor;

//@@viewOn:helpers
//@@viewOff:helpers

//@@viewOn:classNames

const Wrapper = styled('div')`
  display: flex;
  position: fixed;
  bottom: 0%;
`;
const TextAreaWrapper = styled('div')`
  margin-left: 35px;
  
  min-width: calc(100% - 48px);
`;
const SendIconWrapper = styled('div')`
  margin-left: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
  max-width: 20px;
  margin-bottom: 5px;
  cursor: pointer;
  & > span {
    cursor: pointer;
    font-size: 20px;
  }
`;
//@@viewOff:classNames
