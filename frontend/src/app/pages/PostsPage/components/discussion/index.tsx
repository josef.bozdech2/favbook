import React from 'react';
import DiscussionWrapper from './discussion';
import MessageEditor from './message-editor';
import { styled } from '@mui/system';
import { DiscussionListProps } from './types';
import { Grid, List } from '@mui/material';

interface Props extends DiscussionListProps {
  onSave: (message: string | null) => Promise<void>;
}

export const DiscussionLoader = (props: Props) => {
  async function handleSave(message) {
    await props.onSave(message);
  }

  return (
    <Grid container item xs={3}>
      <List
        sx={{
          height: '70vh',
        }}
      >
        <Wrapper>
          <DiscussionWrapper {...props} />
          <MessageEditor handleSave={handleSave} />
        </Wrapper>
      </List>
    </Grid>
  );
};

export default DiscussionLoader;

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
`;
