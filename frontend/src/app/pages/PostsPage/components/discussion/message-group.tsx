import { styled } from '@mui/system';
import { Message } from 'types/response/MessageResponse';
import DiscussionMessage from './message';

export const DiscussionMessageGroup = props => {
  function _renderMessages(data: Message[], isOwnMessage) {
    return data.map(item => (
      <DiscussionMessage key={item.id} {...item} isOwnMessage={isOwnMessage} />
    ));
  }
  const isOwnMessage = props.data[0]?.user.id === props.authorId;
  return (
    <Wrapper isOwnMessage={isOwnMessage}>
      <SenderMessage isOwnMessage={isOwnMessage}>
        {`${props.data[0]?.user.name} ${props.data[0]?.user.surname}`}
      </SenderMessage>
      {_renderMessages(props.data, isOwnMessage)}
    </Wrapper>
  );
};

export default DiscussionMessageGroup;

interface WithOwnMessageType {
  readonly isOwnMessage: boolean;
}

const Wrapper = styled('div')<WithOwnMessageType>`
  clear: both;
  font-size: 12px;
  float: ${props => (props?.isOwnMessage ? 'right' : 'left')};
`;
const SenderMessage = styled('div')<WithOwnMessageType>`
  float: ${props => (props?.isOwnMessage ? 'right' : 'left')};
  margin-bottom: 3px;
`;
