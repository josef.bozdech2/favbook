import { styled } from '@mui/system';
import { NoDataMessage } from 'app/components/NoDataMessage';
import { useEffect, useRef } from 'react';
import { Message } from 'types/response/MessageResponse';
import DiscussionMessageGroup from './message-group';
import { DiscussionListProps } from './types';

export const DiscussionWrapper = (props: DiscussionListProps) => {
  let _messagesEnd = useRef<null | HTMLDivElement>(null);

  useEffect(() => _handleScrollIntoView(), []);

  function _handleScrollIntoView() {
    _messagesEnd?.current?.scrollIntoView({ behavior: 'smooth' });
  }
  function _renderMessageGroups(data) {
    let iterationIndex = 0;
    let prevUserId = null;
    const groupData = data
      .sort((a, b) =>
        a?.timestamp < b?.timestamp ? -1 : a?.timestamp > b?.timestamp ? 1 : 0,
      )
      .reduce((r, a) => {
        if (prevUserId !== a.user.id) iterationIndex += 1;
        prevUserId = a.user.id;
        r[iterationIndex] = r[iterationIndex] || [];
        r[iterationIndex].push(a);
        return r;
      }, []);
    return groupData.map((groupItem: Message[] | null, index) => (
      <DiscussionMessageGroup
        key={index}
        data={groupItem}
        authorId={props.authorId}
      />
    ));
  }
  if (!props.data) return <NoDataMessage message={'No messages yet'} />;

  return (
    <Wrapper>
      {_renderMessageGroups(props.data)}
      <div
        style={{ float: 'left', clear: 'both' }}
        ref={el => {
          _messagesEnd.current = el;
        }}
      ></div>
    </Wrapper>
  );
};

export default DiscussionWrapper;

const Wrapper = styled('div')`
  max-width: 500px;
  max-height: 700px;
  padding: 0 8px;
  overflow-y: scroll;
`;
