import { Message } from "types/response/MessageResponse";

export interface DiscussionMessageType {
  id: string;
  authorId: string;
  authorName: string;
  message: string;
  updatedTime: string;
  createdTime: string;
  discussionTopicId: string;
  replyToDiscussionMessageId: string;
}

export interface DiscussionListProps {
  data: Message[];
  authorId: string | null;
}
