import { styled } from '@mui/system';
import { Message } from 'types/response/MessageResponse';


interface Props extends Message {
  isOwnMessage: boolean;
}

export const DiscussionMessage = (props: Props) => {
  return <Wrapper isOwnMessage={props.isOwnMessage}>{props.content}</Wrapper>;
};

export default DiscussionMessage;

interface WithOwnMessageType {
  readonly isOwnMessage: boolean;
}

const Wrapper = styled('div')<WithOwnMessageType>`
  display: inline-block;
  margin-bottom: 2px;
  clear: both;
  padding: 7px 13px;
  font-size: 12px;
  border-radius: 15px;
  line-height: 1.4;
  word-break: break-word;
  ${props =>
    props?.isOwnMessage &&
    `
      float: right;
      background-color: #1998e6;
      color: #fff;
      border-bottom-right-radius: 5px;
      border-top-right-radius: 5px;
    `}
  ${props =>
    !props?.isOwnMessage &&
    `
      float: left;
      background-color: #ddd;
      border-bottom-left-radius: 5px;
      border-top-left-radius: 5px;
    `}
`;

//@@viewOff:classNames
