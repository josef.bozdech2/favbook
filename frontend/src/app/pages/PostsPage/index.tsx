import { CssBaseline, Grid } from '@mui/material';
import { Box } from '@mui/system';
import { NavBar } from 'app/components/NavBar';
import { useAuthContext } from 'contexts/authContext';
import { useFriendlistContext } from 'contexts/friendListContext';
import useMessageList from 'hooks/effects/useMessageList';
import useMessageReceiving from 'hooks/effects/useMessageReceiving';
import React, { useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { Message } from 'types/response/MessageResponse';
import { EVENT_MESSAGES } from 'utils/socketMessageEnum';
import DiscussionLoader from './components/discussion';
import Friendlist from './components/friendlist';
import { Posts } from './components/posts';
import { useFriendList } from './hooks/useFriendList';

export type MessageListWrapper = {
  messageList: Message[];
};
export const PostsPage = () => {
  const [messages, setMessages] = React.useState<Message[]>([]);
  const [selectedChat, setSelectedChat] = useState<string>('');
  const { socket, userId } = useAuthContext()!;
  const { friendList } = useFriendlistContext()!;
  useFriendList(socket);
  useMessageReceiving(socket, setMessages, selectedChat, userId);

  useMessageList(setMessages, selectedChat, socket);
  async function handleMessageSubmit(message: string | null): Promise<void> {
    await socket?.emit(EVENT_MESSAGES.newMessage, {
      friendId: selectedChat,
      message,
      userId,
    });
  }

  return (
    <Grid container component="main" sx={{ height: '100%' }}>
      <CssBaseline />
      <Helmet>
        <title>Home Page</title>
        <meta name="description" content="Posts" />
      </Helmet>
      <NavBar page="posts" />
      <Box
        sx={{
          paddingLeft: '2%',
          paddingRight: '2%',
          paddingTop: '12px',
          flexGrow: 1,
        }}
      >
        <Grid container>
          <Friendlist
            friendList={friendList}
            socket={socket}
            selectedChat={selectedChat}
            setSelectedChat={setSelectedChat}
          />
          <Posts />
          <DiscussionLoader
            authorId={userId}
            data={messages}
            onSave={handleMessageSubmit}
          />
        </Grid>
      </Box>
    </Grid>
  );
};
