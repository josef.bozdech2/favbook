import { Grid, List, Paper } from '@mui/material';
import React from 'react';

export const UserList = props => {
  const { headerName, renderUsers } = props;
  return (
    <Grid item xs={3}>
      <Grid item xs={11} md={11}>
        <List>
          <Paper
            sx={{
              height: 60,
              lineHeight: '60px',
              marginTop: '-7px',
              marginLeft: '15px',
            }}
            elevation={0}
          >
            {headerName}
          </Paper>
          {renderUsers}
        </List>
      </Grid>
    </Grid>
  );
};
