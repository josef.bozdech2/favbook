/**
 *
 * UserSearchPage
 *
 */
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import AdminPanelSettingsOutlinedIcon from '@mui/icons-material/AdminPanelSettingsOutlined';
import BlockIcon from '@mui/icons-material/Block';
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import FaceTwoToneIcon from '@mui/icons-material/FaceTwoTone';
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import RemoveModeratorIcon from '@mui/icons-material/RemoveModerator';
import {
  Avatar,
  Box,
  Button,
  CssBaseline,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
} from '@mui/material';
import { NavBar } from 'app/components/NavBar';
import { useGetFriendshipRequests } from 'hooks/requests/useGetFriendshipRequests';
import * as React from 'react';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { Socket } from 'socket.io-client';
import axios from 'axios';
import { EVENT_MESSAGES } from 'utils/socketMessageEnum';
import { useAuthContext } from '../../../contexts/authContext';
import { useGetNonRelatedUserList } from '../../../hooks/requests/useGetNonRelatedUserList';
import { existingRoles, isRole } from '../../../utils/isRole';
import { UserList } from './components/userList';
import { useFriendList } from '../PostsPage/hooks/useFriendList';
import { Role, UserWithRolesAndRelation } from 'types/response/UserWithStatusType';
interface Props {}
// TODO Define somewhere else
const RELATIONS_ENUM = {
  add_friend: 1,
  remove_friend: 2,
  accept_request: 3,
  deny_request: 4,
  block_user: 5,
  remove_block: 6,
};
export function UserSearchPage(props: Props) {
  const { userId, roles, socket, setAuthState } = useAuthContext()!;
  useFriendList(socket);
  const { status: friendRequestStatus, data: friendRequestData } =
    useGetFriendshipRequests({
      id: userId,
    });
  const [searchFieldValue, setSearchFieldValue] = React.useState('');
  const [tooShortSearchString, setTooShortSearchString] = React.useState(false);

  const {
    data: nonRelatedUserData,
    mutateAsync: searchNonRelatedUsersBySearchString,
  } = useGetNonRelatedUserList({ id: userId });

  useEffect(() => {
    searchNonRelatedUsersBySearchString('');
  }, [searchNonRelatedUsersBySearchString]);

  const handleSearchFieldChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setSearchFieldValue(event.target.value);
  };

  return (
    <Grid
      container
      component="main"
      sx={{ height: '100%', paddingTop: '12px' }}
    >
      <CssBaseline />
      <Helmet>
        <title>User Search</title>
        <meta name="description" content="User search" />
      </Helmet>
      <NavBar page="search" />
      <Grid container sx={{ marginTop: '10px' }}>
        <Grid item xs={3}>
          <Grid item xs={11} md={11} sx={{}}>
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
              {!tooShortSearchString ? (
                <TextField
                  id="outlined-multiline-flexible"
                  label="User search"
                  multiline
                  maxRows={4}
                  value={searchFieldValue}
                  placeholder="Search user"
                  onChange={handleSearchFieldChange}
                  sx={{ marginLeft: '10px' }}
                />
              ) : (
                <TextField
                  error
                  helperText="Search string must be at least 3 characters long."
                  id="outlined-multiline-flexible"
                  label="User search"
                  multiline
                  maxRows={4}
                  value={searchFieldValue}
                  placeholder="Search user"
                  onChange={handleSearchFieldChange}
                  sx={{ marginLeft: '10px' }}
                />
              )}
              <Button
                variant="contained"
                onClick={() => {
                  searchFieldValue.length < 3
                    ? setTooShortSearchString(true)
                    : setTooShortSearchString(false);
                  searchNonRelatedUsersBySearchString(searchFieldValue);
                }}
                sx={{ marginLeft: '10px' }}
              >
                Search
              </Button>
            </Box>
          </Grid>
          <Grid item xs={11} md={11}>
            <List>
              {nonRelatedUserData !== undefined
                ? renderUsers(
                    0,
                    nonRelatedUserData?.data?.userList,
                    roles,
                    socket,
                    setAuthState,
                  )
                : null}
            </List>
          </Grid>
        </Grid>
        <UserList
          headerName={'Friends'}
          renderUsers={renderUsers(
            friendRequestStatus,
            friendRequestData !== undefined
              ? friendRequestData?.data?.data?.currentFriends
              : [],
            roles,
            socket,
            setAuthState,
          )}
        />
        <UserList
          headerName={'Pending requests'}
          renderUsers={renderUsers(
            friendRequestStatus,
            friendRequestData !== undefined
              ? friendRequestData?.data?.data?.friendshipRequestingUsers
              : [],
            roles,
            socket,
            setAuthState,
          )}
        />
        <UserList
          headerName={'Blocked users'}
          renderUsers={renderUsers(
            friendRequestStatus,
            friendRequestData !== undefined
              ? friendRequestData?.data?.data?.blockedUsers
              : [],
            roles,
            socket,
            setAuthState,
          )}
        />
      </Grid>
    </Grid>
  );
}

const renderUsers = (
  status,
  users: UserWithRolesAndRelation[] | null | undefined,
  currentUserRoles: Role[] | null,
  socket: Socket | null,
  setAuthState,
) => {
  return (
    users &&
    users?.map(item =>
      renderSingleUser(item, currentUserRoles, socket, setAuthState),
    )
  );
};

const renderSingleUser = (
  user: UserWithRolesAndRelation,
  currentUserRoles: Role[] | null,
  socket: Socket | null,
  setAuthState,
) => {
  return (
    <ListItem
      key={user.id}
      secondaryAction={renderActionIcons(
        user,
        currentUserRoles,
        socket,
        setAuthState,
      )}
    >
      <ListItemAvatar>
        <Avatar>
          <FaceTwoToneIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText primary={`${user.name} ${user.surname}`} />
    </ListItem>
  );
};

const renderActionIcons = (
  user: UserWithRolesAndRelation,
  currentUserRoles: Role[] | null,
  socket: Socket | null,
  setAuthState,
) => {
  if (user.status === 0) {
    return (
      <>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.add_friend,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="add"
        >
          <PersonAddAltOutlinedIcon />
        </IconButton>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.block_user,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="block"
        >
          <BlockIcon />
        </IconButton>
      </>
    );
  }
  if (user.status === 1) {
    return (
      <>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.remove_friend,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="add"
        >
          <PersonRemoveIcon />
        </IconButton>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.block_user,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="block"
        >
          <BlockIcon />
        </IconButton>
        <AdminIcon currentUserRoles={currentUserRoles} renderedUser={user} />
      </>
    );
  }
  if (user.status === 2) {
    return (
      <>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.remove_friend,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="add"
        >
          <PersonAddAlt1Icon />
        </IconButton>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.block_user,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="block"
        >
          <BlockIcon />
        </IconButton>
      </>
    );
  }
  if (user.status === 3) {
    return (
      <>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.accept_request,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="add"
        >
          <CheckIcon />
        </IconButton>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.deny_request,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="add"
        >
          <ClearIcon />
        </IconButton>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.block_user,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="block"
        >
          <BlockIcon />
        </IconButton>
      </>
    );
  }
  if (user.status === 4) {
    return (
      <>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.add_friend,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="add"
        >
          <PersonAddAltOutlinedIcon />
        </IconButton>
        <IconButton
          onClick={() =>
            performUserAction(
              user.id,
              RELATIONS_ENUM.remove_block,
              socket,
              setAuthState,
            )
          }
          edge="end"
          aria-label="block"
        >
          <RemoveModeratorIcon />
        </IconButton>
      </>
    );
  }
};

type EventType = typeof RELATIONS_ENUM[keyof typeof RELATIONS_ENUM];

async function performUserAction(
  id: string,
  event_type: EventType,
  socket: Socket | null,
  setAuthState,
): Promise<void> {
  await axios
    .post('/relations/update', {
      friendId: id,
      relationType: event_type,
    })
    .then(() => {
      if (event_type === RELATIONS_ENUM.accept_request) {
        socket?.emit(EVENT_MESSAGES.acceptedFriendship, { id });
      }
      if (event_type === RELATIONS_ENUM.block_user)
        socket?.emit(EVENT_MESSAGES.blockedFriend, { id });
      if (event_type === RELATIONS_ENUM.remove_friend)
        socket?.emit(EVENT_MESSAGES.removedFriend, { id });
    });
}

function changeUserAdminRole(
  id: string,
  isAddRole: boolean,
  socket: Socket | null,
  setAuthState,
): void {
  axios.post('/users/admin', {
    userId: id,
    isAddRole,
  });
}

const AdminIcon = props => {
  const { currentUserRoles, renderedUser, socket, setAuthState } = props;
  const isCurrentUserAdmin: boolean = isRole(
    currentUserRoles,
    existingRoles.admin,
  );
  const isRenderedUserAdmin: boolean = isRole(
    renderedUser.roles,
    existingRoles.admin,
  );

  if (isCurrentUserAdmin) {
    return (
      <IconButton
        onClick={() =>
          changeUserAdminRole(
            renderedUser.id,
            !isRenderedUserAdmin,
            socket,
            setAuthState,
          )
        }
        edge="end"
        aria-label="add"
      >
        {isRenderedUserAdmin ? (
          <AdminPanelSettingsIcon />
        ) : (
          <AdminPanelSettingsOutlinedIcon />
        )}
      </IconButton>
    );
  }
  return null;
};
