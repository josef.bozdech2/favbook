/**
 *
 * Asynchronously loads the component for UserSearchPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const UserSearchPage = lazyLoad(
  () => import('./index'),
  module => module.UserSearchPage,
);
