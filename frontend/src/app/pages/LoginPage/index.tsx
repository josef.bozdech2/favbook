import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import LoginAlert from 'app/components/LoginAlert';
import { NavBar } from 'app/components/NavBar';
import { AxiosResponse } from 'axios';
import { useLoginMutation } from 'hooks/requests/useLoginMutation';
import React, { memo, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { io, Socket } from 'socket.io-client';
import styled from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';
import { UserWithRoles } from 'types/response/UserWithStatusType';
import { AuthContextType, useAuthContext } from '../../../contexts/authContext';
import FavntIcon from './assets/fav-space-favnt.png';

interface Props {}

type UseLoginMutationType = {
  cookie: string;
  data: UserWithRoles;
  token: string;
};

export const LoginPage = memo((props: Props) => {
  const url: string | undefined = String(process.env.REACT_APP_SERVER_URL);
  const { setAuthState } = useAuthContext()!;
  const [openAlert, setOpenAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const loginMutation = useLoginMutation(
    setOpenAlert,
    setAlertMessage,
    async (dtoOut: AxiosResponse<UseLoginMutationType> | void) => {
      if (dtoOut) {
        if (dtoOut?.status === 200) {
          let newState: Partial<AuthContextType> = {
            isAuth: true,
            userName: `${dtoOut.data.data.name} ${dtoOut.data.data.surname}`,
            userId: dtoOut.data.data.id,
            roles: dtoOut.data.data.roles,
            jwt: dtoOut.data.token,
          };
          const newSocket = io(url, {
            auth: { token: dtoOut.data.token },
            upgrade: false,
          });
          createSocketConnection(newSocket, dtoOut.data.token);
          newState = { ...newState, socket: newSocket };
          await setAuthState(newState);
        }
      }
    },
  );

  const createSocketConnection = async (newSocket: Socket, token: string) => {
    await newSocket.emit('userLogin');
  };
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const loginParams = {
      email: String(data.get('email')),
      password: String(data.get('password')),
    };
    try {
      loginMutation.mutate(loginParams);
    } catch (error) {
    }
  };

  return (
    <>
      <Helmet>
        <title>Login</title>
        <meta name="description" content="Login page" />
      </Helmet>
      <NavBar page="login" />
      <Wrapper>
        <Grid container component="main" sx={{ height: '100%' }}>
          <CssBaseline />
          <Grid
            item
            xs={false}
            sm={4}
            md={7}
            sx={{
              backgroundImage: `url(${FavntIcon})`,
              backgroundRepeat: 'no-repeat',
              backgroundColor: t =>
                t.palette.mode === 'light'
                  ? t.palette.grey[0]
                  : t.palette.grey[900],
              backgroundSize: 'contain',
              backgroundPosition: 'center',
            }}
          />
          <Grid
            item
            xs={12}
            sm={8}
            md={5}
            component={Paper}
            elevation={6}
            square
          >
            <Box
              sx={{
                my: 8,
                mx: 4,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <Box
                component="form"
                noValidate
                onSubmit={handleSubmit}
                sx={{ mt: 1 }}
              >
                {openAlert ? (
                  <LoginAlert
                    setOpenAlert={setOpenAlert}
                    message={alertMessage}
                  />
                ) : null}
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Sign In
                </Button>
                <Grid container>
                  <Grid item>
                    <Link to="/signup">"Don't have an account? Sign Up"</Link>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Wrapper>
    </>
  );
});

const Wrapper = styled.div`
  height: calc(100vh - ${StyleConstants.NAV_BAR_HEIGHT});
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;
