import { createTheme, ThemeProvider } from '@mui/material/styles';
import { StyledEngineProvider } from '@mui/styled-engine-sc';
import * as React from 'react';
import { CookiesProvider } from 'react-cookie';
import { AuthContextProvider } from '../contexts/authContext';
import { ColorModeContext } from './colorModeContext';
import { AppRouter } from './router';

export function App() {
  const [mode, setMode] = React.useState<'light' | 'dark'>('light');
  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode(prevMode => (prevMode === 'light' ? 'dark' : 'light'));
      },
      theme: theme,
    }),
    [],
  );

  return (
    <StyledEngineProvider injectFirst>
      <ColorModeContext.Provider value={colorMode}>
        <CookiesProvider>
          <ThemeProvider theme={theme}>
            <AuthContextProvider>
              <AppRouter />
            </AuthContextProvider>
          </ThemeProvider>
        </CookiesProvider>
      </ColorModeContext.Provider>
    </StyledEngineProvider>
  );
}
