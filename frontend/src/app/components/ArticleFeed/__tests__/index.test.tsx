import * as React from 'react';
import { render } from '@testing-library/react';

import { ArticleFeed } from '..';

describe('<ArticleFeed  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<ArticleFeed />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
