/**
 *
 * Asynchronously loads the component for ArticleFeed
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ArticleFeed = lazyLoad(
  () => import('./index'),
  module => module.ArticleFeed,
);
