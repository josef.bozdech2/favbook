import React, { useContext, useState } from 'react';
import { ThemeProvider } from '@emotion/react';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import LogoutIcon from '@mui/icons-material/Logout';
import SearchIcon from '@mui/icons-material/Search';
import SettingsIcon from '@mui/icons-material/Settings';
import { IconButton } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { ColorModeContext } from 'app/colorModeContext';
import { useAuthContext } from 'contexts/authContext';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import axios from 'axios';
import { EVENT_MESSAGES } from 'utils/socketMessageEnum';
export function Nav({ page }) {
  const theme = useTheme();
  const colorMode = React.useContext(ColorModeContext);
  const { socket, setAuthState, isAuth } = useAuthContext()!;

  const sendLogout = async () => {
    await axios.post('/logout').then(response => {
      if (response.status === 200) {
        socket?.close();
        axios.defaults.headers.common['Authorization'] = '';
        setAuthState({ isAuth: false });
      }
    });
  };
  return (
    <Wrapper>
      {isAuth ? (
        <ThemeProvider theme={colorMode.theme}>
          {/* <IconButton
            sx={{ ml: 1 }}
            onClick={colorMode.toggleColorMode}
            color="warning"
          >
            {theme.palette.mode === 'dark' ? (
              <Brightness7Icon />
            ) : (
              <Brightness4Icon />
            )}
          </IconButton> */}
          {/* <IconButton sx={{ ml: 1 }} onClick={() => {}} color="warning">
            <StyledLink to="/home">
              <SettingsIcon />
            </StyledLink>
          </IconButton> */}
          <IconButton sx={{ ml: 1 }} onClick={() => {}} color="warning">
            <StyledLink to="/search">
              <SearchIcon />
            </StyledLink>
          </IconButton>
          <IconButton sx={{ ml: 1 }} onClick={sendLogout} color="warning">
            <StyledLink to="/">
              <LogoutIcon />
            </StyledLink>
          </IconButton>
        </ThemeProvider>
      ) : null}
    </Wrapper>
  );
}
const Wrapper = styled.nav`
  display: flex;
  margin-right: -1rem;
`;

const StyledLink = styled(Link)`
  display: flex;
  margin-right: -1rem;
  color: white;
`;
