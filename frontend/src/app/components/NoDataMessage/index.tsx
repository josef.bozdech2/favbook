import * as React from 'react';
import styled from 'styled-components/macro';

interface Props {
  message: string;
}

export function NoDataMessage(props: Props) {
  if (props.message) return <Div>{props.message}</Div>;
  return (
    <Div>
      There is <strong>no data</strong> for specified filter settings
    </Div>
  );
}

const Div = styled.div`
  width: 100%;
  color: rgba(0, 0, 0, 0.8);
  padding: 32px 40px;
`;
