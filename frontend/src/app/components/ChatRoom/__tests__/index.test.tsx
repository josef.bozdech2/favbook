import * as React from 'react';
import { render } from '@testing-library/react';

import { ChatRoom } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<ChatRoom  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<ChatRoom />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
