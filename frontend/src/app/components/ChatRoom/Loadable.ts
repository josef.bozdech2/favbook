/**
 *
 * Asynchronously loads the component for ChatRoom
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ChatRoom = lazyLoad(
  () => import('./index'),
  module => module.ChatRoom,
);
