import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { GlobalStyle } from '../styles/global-styles';
import { LoginPage } from './pages/LoginPage/Loadable';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { PostsPage } from './pages/PostsPage/Loadable';
import { SignUpPage } from './pages/SignUpPage/Loadable';
import { UserSearchPage } from './pages/UserSearchPage/Loadable';
import { useAuthContext } from '../contexts/authContext';
import { FriendlistContextProvider } from 'contexts/friendListContext';

export function AppRouter() {
  const { i18n } = useTranslation();
  const { isAuth } = useAuthContext()!;
  function renderAuth() {
    return (
      <>
        <FriendlistContextProvider>
          <Route
            exact
            path={process.env.PUBLIC_URL + '/'}
            component={PostsPage}
          />
          <Route
            exact
            path={process.env.PUBLIC_URL + '/search'}
            component={UserSearchPage}
          />
          <Redirect to="/" />
        </FriendlistContextProvider>
      </>
    );
  }
  function renderNotAuth() {
    return (
      <>
        <Route
          exact
          path={process.env.PUBLIC_URL + '/'}
          component={LoginPage}
        />
        <Route
          exact
          path={process.env.PUBLIC_URL + '/signup'}
          component={SignUpPage}
        />
        <Redirect to="/" />
      </>
    );
  }

  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - Favn’t"
        defaultTitle="Favn’t"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="A Favn’t application" />
      </Helmet>

      <Switch>
        {isAuth ? renderAuth() : renderNotAuth()}
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </BrowserRouter>
  );
}
