import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import React, { useContext, useState } from 'react';
import { Socket } from 'socket.io-client';
import { Role } from 'types/response/UserWithStatusType';

export type AuthContextType = {
  isAuth: boolean;
  userId: string | null;
  userName: string | null;
  roles: Role[] | null;
  jwt: string | null;
  socket: Socket | null;
  setAuthState: (newState: Partial<AuthContextType>) => void;
};
export const AuthContext = React.createContext<AuthContextType | null>(null);

const defaultAuthContextValue: AuthContextType = {
  isAuth: false,
  userId: null,
  userName: null,
  roles: null,
  jwt: null,
  socket: null,
  setAuthState: () => {},
};

export const AuthContextProvider = props => {
  axios.defaults.baseURL = process.env.REACT_APP_SERVER_URL;
  axios.defaults.withCredentials = true;
  const [authState, setAuthState] = useState<AuthContextType>(
    defaultAuthContextValue,
  );
  //TODO: useEffect for setInterceptorsTo()
  const onResponse = (response: AxiosResponse): AxiosResponse => {
    return response;
  };

  const onResponseError = (error: AxiosError): Promise<AxiosError> => {
    if (error.response?.status === 401 && authState.isAuth === true) {
      setAuthState({ ...authState, isAuth: false });
      authState?.socket?.close();
      alert(
        'You have beed disconnected! Jokes on you! You must relog now! HGAHAHASTGHGASHAHASDHA',
      );
    }
    return Promise.reject(error);
  };

  function setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
    axiosInstance.interceptors.response.use(onResponse, onResponseError);
    return axiosInstance;
  }

  setupInterceptorsTo(axios);

  return (
    <AuthContext.Provider
      value={{
        ...authState,
        setAuthState: newState => setAuthState({ ...authState, ...newState }),
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);
