import React, { useContext, useState } from 'react';
import { User } from 'types/response/UserWithStatusType';

export type FriendlistContextType = {
  friendList: User[];
  setFriendList: (newState: Partial<FriendlistContextType>) => void;
};
export const FriendlistContext =
  React.createContext<FriendlistContextType | null>(null);

const defaultFriendlistContextValue: FriendlistContextType = {
  friendList: [],
  setFriendList: () => {},
};

export const FriendlistContextProvider = props => {
  const [friendlist, setFriendList] = useState(defaultFriendlistContextValue);

  return (
    <FriendlistContext.Provider
      value={{
        ...friendlist,
        setFriendList: newState =>
          setFriendList({ ...friendlist, ...newState }),
      }}
    >
      {props.children}
    </FriendlistContext.Provider>
  );
};

export const useFriendlistContext = () => useContext(FriendlistContext);
