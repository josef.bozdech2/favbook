process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';
import App from '@/app';
import { PrismaClient } from '@prisma/client';
import { container } from 'tsyringe';
import AuthController from './controllers/auth.controller';
import adminUserCheck from './prisma/adminUserCheck';
import { main } from './prisma/onServerStartSeed';
const app = new App();
main();
const adminExists = adminUserCheck();

app.listen();
