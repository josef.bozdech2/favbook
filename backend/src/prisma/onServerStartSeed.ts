import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';

const dataStructure = {
  roleList: [
    {
      role: 'USER',
    },
    {
      role: 'ADMIN',
    },
    {
      role: 'GOD',
    },
  ],
  statusList: [
    { type: 1, typeString: 'friends' },
    { type: 2, typeString: 'requestedFriendship' },
    { type: 3, typeString: 'requestedForFriendship' },
    { type: 4, typeString: 'blockedUser' },
    { type: 5, typeString: 'blockedByUser' },
  ],
};

export interface User {
  id?: string;
  email: string;
  password: string;
  name: string;
  surname: string;
}

export interface Role {
  id: string;
  role: string;
}

export interface Status {
  id?: string;
  type: number;
  typeString: string;
}
const prisma = new PrismaClient();

export async function main() {
  // Generate STATUS
  const states = await Promise.all(dataStructure.statusList.map(async _status => await createStatus(_status)));
  // Generate ROLES
  const roles = await Promise.all(dataStructure.roleList.map(async _role => await createRole(_role)));
}

async function createRole(userRole: Omit<Role, 'id'>) {
  return prisma.role.upsert({
    create: {
      ...userRole,
    },
    update: {
      ...userRole,
    },
    where: {
      role: userRole.role,
    },
  });
}

async function createStatus(_status: Status) {
  return prisma.status.upsert({
    create: {
      type: _status.type,
      typeString: _status.typeString,
    },
    update: {
      type: _status.type,
      typeString: _status.typeString,
    },
    where: {
      id: _status?.typeString || '',
    },
  });
}

main()
  .catch(e => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
