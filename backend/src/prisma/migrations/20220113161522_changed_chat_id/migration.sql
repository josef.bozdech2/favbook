/*
  Warnings:

  - You are about to drop the column `relationshipId` on the `Message` table. All the data in the column will be lost.
  - You are about to drop the column `likes` on the `Post` table. All the data in the column will be lost.
  - You are about to drop the `Relationships` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[role]` on the table `Role` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `chatId` to the `Message` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `Message` DROP FOREIGN KEY `Message_ibfk_2`;

-- DropForeignKey
ALTER TABLE `Relationships` DROP FOREIGN KEY `Relationships_ibfk_3`;

-- DropForeignKey
ALTER TABLE `Relationships` DROP FOREIGN KEY `Relationships_ibfk_1`;

-- DropForeignKey
ALTER TABLE `Relationships` DROP FOREIGN KEY `Relationships_ibfk_2`;

-- AlterTable
ALTER TABLE `Message` DROP COLUMN `relationshipId`,
    ADD COLUMN `chatId` VARCHAR(191) NOT NULL,
    ADD COLUMN `reactingToId` VARCHAR(191);

-- AlterTable
ALTER TABLE `Post` DROP COLUMN `likes`,
    ADD COLUMN `likesCount` INTEGER NOT NULL DEFAULT 0,
    MODIFY `content` LONGTEXT NOT NULL,
    MODIFY `timestamp` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    MODIFY `isAnnouncement` BOOLEAN NOT NULL DEFAULT false;

-- DropTable
DROP TABLE `Relationships`;

-- CreateTable
CREATE TABLE `Friendlist` (
    `id` VARCHAR(191) NOT NULL,
    `userId1` VARCHAR(191) NOT NULL,
    `userId2` VARCHAR(191) NOT NULL,
    `statusId` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `_postLikes` (
    `A` VARCHAR(191) NOT NULL,
    `B` VARCHAR(191) NOT NULL,

    UNIQUE INDEX `_postLikes_AB_unique`(`A`, `B`),
    INDEX `_postLikes_B_index`(`B`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `Role.role_unique` ON `Role`(`role`);

-- AddForeignKey
ALTER TABLE `Friendlist` ADD FOREIGN KEY (`userId1`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Friendlist` ADD FOREIGN KEY (`userId2`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Friendlist` ADD FOREIGN KEY (`statusId`) REFERENCES `Status`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Message` ADD FOREIGN KEY (`chatId`) REFERENCES `Friendlist`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Message` ADD FOREIGN KEY (`reactingToId`) REFERENCES `Message`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_postLikes` ADD FOREIGN KEY (`A`) REFERENCES `Post`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_postLikes` ADD FOREIGN KEY (`B`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
