import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';

const dataStructure = {
  userList: [
    { password: 'test', name: 'Luqman', surname: 'Sharpe' },
    { password: 'test', name: 'Bruno', surname: 'Todd' },
    { password: 'test', name: 'Asiya', surname: 'Hollis' },
    { password: 'test', name: 'JoIzaac', surname: 'Coatessef4' },
    { password: 'test', name: 'Alicja', surname: 'Zamora' },
    { password: 'test', name: 'Rogan', surname: 'Markham' },
  ],
  roleList: [
    {
      role: 'USER',
    },
    {
      role: 'ADMIN',
    },
    {
      role: 'GOD',
    },
  ],
  postList: [],
  statusList: [
    { type: 1, typeString: 'friends' },
    { type: 2, typeString: 'requestedFriendship' },
    { type: 3, typeString: 'requestedForFriendship' },
    { type: 4, typeString: 'blockedUser' },
    { type: 5, typeString: 'blockedByUser' },
  ],
};

export interface User {
  id?: string;
  email: string;
  password: string;
  name: string;
  surname: string;
}

export interface Role {
  id: string;
  role: string;
}

export interface Status {
  id?: string;
  type: number;
  typeString: string;
}
const prisma = new PrismaClient();

export async function main() {
  // Generate STATUS
  const states = await Promise.all(dataStructure.statusList.map(async _status => await createStatus(_status)));
  // Generate ROLES
  const roles = await Promise.all(dataStructure.roleList.map(async _role => await createRole(_role)));
  // Generate USERS
  const userRole = await prisma.role.findFirst({
    where: {
      role: 'USER',
    },
  });
  const users = await Promise.all(dataStructure.userList.map(async _user => await createUser(_user, userRole)));
}

async function createRole(userRole: Omit<Role, 'id'>) {
  const _userRole = await prisma.role.findFirst({
    where: {
      role: userRole.role,
    },
  });
  return prisma.role.upsert({
    create: {
      ...userRole,
    },
    update: {
      ...userRole,
    },
    where: {
      id: _userRole?.id || '',
    },
  });
}

async function createUser(_user: Omit<User, 'email'>, userRole: Role | null) {
  const hashedPassword = await bcrypt.hash(_user.password, 10);
  const user: User = { ..._user, email: `${_user.name.toLowerCase()}.${_user.surname.toLowerCase()}@gmail.com`, password: hashedPassword };
  const existingUser = await prisma.user.findFirst({
    where: {
      email: user.email,
    },
  });
  return prisma.user.upsert({
    create: {
      ...user,
      posts: {
        createMany: {
          data: [createPost()],
        },
      },
      roles: {
        connect: {
          id: userRole?.id,
        },
      },
    },
    update: {
      ...user,
      posts: {
        createMany: {
          data: [createPost()],
        },
      },
    },
    where: {
      id: existingUser?.id || '',
    },
  });
}

async function createStatus(_status: Status) {
  return prisma.status.upsert({
    create: {
      type: _status.type,
      typeString: _status.typeString,
    },
    update: {
      type: _status.type,
      typeString: _status.typeString,
    },
    where: {
      id: _status?.id || '',
    },
  });
}
function createPost() {
  return {
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at justo vel est dignissim vulputate sit amet in est. Donec vitae pharetra ipsum. Quisque sed commodo elit. Quisque mauris elit, molestie vel bibendum nec, luctus et ipsum. Mauris bibendum, arcu et blandit tempor, tellus erat dapibus odio, id gravida sem quam quis neque. Aenean molestie, enim in sollicitudin tristique, neque tellus tincidunt mauris, sed ultricies lorem mauris vitae dolor. Quisque dignissim eleifend quam, a lacinia nisi cursus ac. Pellentesque sodales, odio congue faucibus elementum, mauris odio euismod ante, vel viverra leo orci malesuada augue. Maecenas vel vestibulum eros, non dignissim nibh. Nullam suscipit blandit ante, sed consectetur velit scelerisque a.',
    likesCount: Math.floor(Math.hypot(Math.random() * 100)),
    timestamp: new Date(),
    isAnnouncement: false,
  };
}

main()
  .catch(e => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
