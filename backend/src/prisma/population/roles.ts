export const roles = [
  {
    role: 'user',
  },
  {
    role: 'admin',
  },
  {
    role: 'moderator',
  },
];
