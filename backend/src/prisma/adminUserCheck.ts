import { User, UserWithPassword } from '@/types/response/UserWithStatusType';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';
const adminUserCheck = async (): Promise<void> => {
  const prisma = new PrismaClient();
  const user = await prisma.user.findFirst({
    where: {
      roles: {
        some: {
          role: 'ADMIN',
        },
      },
    },
  });
  prisma.$disconnect();
  if (user) return;
  generateAdmin();
  return;
};

const generateAdmin = async (): Promise<void> => {
  const prisma = new PrismaClient();
  const name = Math.random().toString(36).slice(-8);
  const nameExists: User | null = await prisma.user.findFirst({
    where: {
      email: `${name}@favnt.com`,
    },
  });
  if (nameExists) {
    prisma.$disconnect();
    generateAdmin();
  }
  const password = Math.random().toString(36).slice(-8);
  const user: User = await prisma.user.create({
    select: {
      id: true,
      name: true,
      surname: true,
      email: true,
    },
    data: {
      email: `${name}@favnt.com`,
      password: await bcrypt.hash(password, 10),
      name: 'Admin',
      surname: 'User',
      roles: {
        connect: {
          role: 'ADMIN',
        },
      },
    },
  });
  const userWithPassword: UserWithPassword = { ...user, password };
  console.log(`email: ${userWithPassword.email} password: ${userWithPassword.password}`);
};

export default adminUserCheck;
