process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';
import 'reflect-metadata';

import { logger, stream } from '@utils/logger';
import config from 'config';
import cors from 'cors';
import express from 'express';
import { container } from 'tsyringe';
import AuthController from './controllers/auth.controller';
import PostsController from './controllers/posts.controller';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import errorMiddleware from './middlewares/error.middleware';
import morgan from 'morgan';
import hpp from 'hpp';
import helmet from 'helmet';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import UsersController from './controllers/users.controller';
import RelationsController from './controllers/relations.controller';
import { Server } from 'http';
import { SocketServer } from './sockets/index.sockets';

class App {
  public app: express.Application;
  public port: string | number;
  public env: string;
  public router;
  public socketServer: SocketServer;

  constructor() {
    this.app = express();
    this.port = process.env.PORT || 5000;
    this.env = process.env.NODE_ENV || 'development';
    this.router = express.Router();
    this.socketServer = new SocketServer();
    this.initializeSwagger();
    this.initializeMiddlewares();
    this.initializeRoutes();
    this.initializeErrorHandling();
  }

  public listen() {
    const server = this.app.listen(this.port, () => {
      logger.info(`=================================`);
      logger.info(`======= ENV: ${this.env} =======`);
      logger.info(`🚀 App listening on the port ${this.port}`);
      logger.info(`=================================`);
    });
    this.initializeSocketServer(server);
  }

  public getServer() {
    return this.app;
  }

  private initializeMiddlewares() {
    this.app.use(morgan(config.get('log.format'), { stream }));
    // this.app.use(cors());
    this.app.use(
      cors({ origin: config.get('cors.origin'), credentials: config.get('cors.credentials'), allowedHeaders: config.get('cors.allowedHeaders') }),
    );
    this.app.use(hpp());
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
  }

  private initializeRoutes() {
    const controllers = [
      container.resolve(AuthController),
      container.resolve(PostsController),
      container.resolve(UsersController),
      container.resolve(RelationsController),
    ];
    controllers.forEach(controller => {
      controller.bindRoutes(this.router);
    });
    this.app.use('/', this.router);
  }

  private initializeSwagger() {
    const options = {
      swaggerDefinition: {
        info: {
          title: 'REST API',
          version: '1.0.0',
          description: 'Example docs',
        },
      },
      apis: ['swagger.yaml'],
    };

    const specs = swaggerJSDoc(options);
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeSocketServer(server: Server) {
    this.socketServer.init(server);
  }
}

export default App;
