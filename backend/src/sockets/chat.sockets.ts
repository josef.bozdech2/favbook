import { PrismaClient } from '.prisma/client';
import c from 'config';
import { Server, Socket } from 'socket.io';
import { UsersMap } from './index.sockets';
import RelationsService from '@/services/relations.service';
import { AuthenticatedSocket } from '@/interfaces/socketAuth.interface';
import { EVENT_MESSAGES } from '@/types/socket/SocketMessageEnum';
import { Relation } from '@/types/response/UserWithStatusType';
import { Message } from '@/types/response/MessageResponse';
import { LoadMoreMessagesRequest } from '@/types/request/MessageRequest';

type MessageUpdate = {
  messageId: string;
  content: string;
};
type NewMessage = {
  friendId: string;
  message: string;
  chatId: string;
  userId: string;
};

export const registerChatSocketService = (socket: AuthenticatedSocket, usersMap: UsersMap, prisma: PrismaClient): void => {
  const relationService = new RelationsService();
  socket.on(EVENT_MESSAGES.newMessage, async ({ friendId, message }: NewMessage) => {
    const relation: Relation | null = await relationService.getFriendshipIdByUserIdAndFriendId(socket.user.id, friendId);
    try {
      if (relation) {
        const a: Message = await prisma.message.create({
          select: {
            id: true,
            chatId: true,
            user: true,
            content: true,
            timestamp: true,
          },
          data: {
            content: message,
            chatId: relation.id,
            userId: socket.user.id,
          },
        });
        sendMessageToAllUserSockets(socket, usersMap, EVENT_MESSAGES.privateMessage, friendId, a);
      }
    } catch (error) {
      console.log({ error });
    }
  });
  socket.on('messageEdit', (message: MessageUpdate) => {
    prisma.message.update({
      where: {
        id: message.messageId,
      },
      data: {
        content: message.content,
      },
    });
  });
  socket.on('messageDelete', (messageId: string) => {
    prisma.message.delete({
      where: {
        id: messageId,
      },
    });
  });

  socket.on(EVENT_MESSAGES.loadMoreMessages, async ({ friendId, offset = 0 }: LoadMoreMessagesRequest) => {
    const relation: Relation | null = await relationService.getFriendshipIdByUserIdAndFriendId(socket.user.id, friendId);
    if (relation) {
      const messageList: Message[] | null = await prisma.message.findMany({
        select: {
          id: true,
          user: true,
          content: true,
          timestamp: true,
          chatId: true,
        },
        skip: offset,
        take: 10,
        orderBy: {
          timestamp: 'desc',
        },
        where: {
          chatId: relation.id,
        },
      });
      socket.emit('getMoreMessages', { messageList });
    }
  });
};

const sendMessageToAllUserSockets = async (
  socket: AuthenticatedSocket,
  usersMap: UsersMap,
  event_message: string,
  friendId: string,
  message: Message,
): Promise<void> => {
  //find all sending users sockets
  const currentUserSockets: string[] = findUserSockets(socket.user.id, usersMap);
  //find all receiving users sockets
  const friendSockets: string[] = findUserSockets(friendId, usersMap);
  //send message to sending user socket
  socket.emit(event_message, { message });
  //send message to all other sending user sockets
  socket.to(currentUserSockets).emit(event_message, { message });
  //send message to receiving user sockets
  socket.to(friendSockets).emit(event_message, { message });
};

const findUserSockets = (userId: string, usersMap: UsersMap): string[] => {
  const userSocketMap = usersMap.get(userId);
  const userSocketArray = userSocketMap?.values() || [];
  const sockets = Array.from(userSocketArray);
  return sockets;
};
