import socketAuthMiddleware from '@/middlewares/socketAuth.middleware';
import UserService from '@/services/users.service';
import { PrismaClient } from '@prisma/client';
import { Server as HttpServerType } from 'http';
import { Server } from 'socket.io';
import { registerChatSocketService } from './chat.sockets';
import { registerFriendListSocketService } from './friendList.sockets';

export type UsersMapValue = Set<string>;
export type UsersMap = Map<string, UsersMapValue>;

export class SocketServer {
  public io: Server | undefined;
  public prisma = new PrismaClient();
  public userService = new UserService();
  public usersMap: UsersMap = new Map();
  public init(server: HttpServerType): void {
    this.io = new Server(server, {
      cors: {
        origin: '*', //FIXME: Please HELP ME, IM DYING => Decide by using NODE_ENV
        methods: ['GET', 'POST'],
      },
    });
    this.io.use(async (socket, next) => {
      await socketAuthMiddleware(socket);
      next();
    });
    this.io.on('connection', async (socket: any): Promise<void> => {
      await registerFriendListSocketService(socket, this.usersMap, this.userService);
      await registerChatSocketService(socket, this.usersMap, this.prisma);
    });
  }
}
