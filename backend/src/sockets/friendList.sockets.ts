import { AuthenticatedSocket } from '@/interfaces/socketAuth.interface';
import UserService from '@/services/users.service';
import { User, UserWithRoles, UserWithStatusAndRoles } from '@/types/response/UserWithStatusType';
import { EVENT_MESSAGES } from '@/types/socket/SocketMessageEnum';
import { PrismaClient } from '@prisma/client';
import { UsersMap } from './index.sockets';

export const registerFriendListSocketService = (socket: AuthenticatedSocket, usersMap: UsersMap, userService: UserService): void => {
  socket.on(EVENT_MESSAGES.userLogin, async () => {
    addConnection(socket, usersMap, socket.user.id);
    //send currently online users to this user
    sendOnlineUsersToNewUser(socket, usersMap, EVENT_MESSAGES.currentlyOnlineUsers, userService);
    sendUpdateToOnlineUsers(socket, usersMap, EVENT_MESSAGES.userConnected, userService);
  });
  socket.on(EVENT_MESSAGES.disconnect, async () => {
    removeConnection(socket, usersMap, socket.user.id);
    sendUpdateToOnlineUsers(socket, usersMap, EVENT_MESSAGES.userDisconnected, userService);
  });
  socket.on(EVENT_MESSAGES.acceptedFriendship, async ({ id }) => {
    sendOnlineUsersToUserOnRelationChange(socket, id, usersMap, EVENT_MESSAGES.acceptedFriendship, userService);
  });
  socket.on(EVENT_MESSAGES.removedFriend, async ({ id }) => {
    sendOnlineUsersToUserOnRelationChange(socket, id, usersMap, EVENT_MESSAGES.removedFriend, userService);
  });
  socket.on(EVENT_MESSAGES.blockedFriend, async ({ id }) => {
    sendOnlineUsersToUserOnRelationChange(socket, id, usersMap, EVENT_MESSAGES.blockedFriend, userService);
  });
};

const addConnection = (socket: AuthenticatedSocket, usersMap: UsersMap, userId: string) => {
  if (!usersMap.has(userId)) {
    const userSocketMap = new Set<string>().add(socket.id);
    usersMap.set(userId, userSocketMap);
  } else {
    const userSocketMap = usersMap.get(userId)?.add(socket.id);
    if (userSocketMap) usersMap.set(userId, userSocketMap);
  }
};

const removeConnection = (socket: AuthenticatedSocket, usersMap: UsersMap, userId: string) => {
  if (!usersMap.has(userId)) {
    throw new Error('User not found in userMap');
  }

  const userSocketSet: Set<string> | undefined = usersMap.get(userId);
  userSocketSet?.delete(socket.id);
  if (userSocketSet !== undefined) usersMap.set(userId, userSocketSet);
  if (userSocketSet?.size === 0) {
    usersMap.delete(userId);
  }
};

const filterOnlineUsers = (usersMap: UsersMap, usersFriendsIdList: string[]) => {
  const onlineUserSockets: string[] = [];
  const onlineUsers = usersFriendsIdList.filter(userId => usersMap.has(userId));
  onlineUsers?.forEach(userId => {
    const userSocketMap = usersMap.get(userId);
    const userSocketArray = userSocketMap?.values() || [];
    const sockets = Array.from(userSocketArray);
    sockets?.forEach(value => onlineUserSockets.push(value));
  });

  return onlineUserSockets;
};

const sendUpdateToOnlineUsers = async (socket: AuthenticatedSocket, usersMap: UsersMap, event_message: string, userService: UserService) => {
  const userFriends: UserWithStatusAndRoles[] | null = await userService.findUsersFriendsByUserId(socket.user.id);
  //send update to friends
  const currentUser: User | null = await userService.findUserById(socket.user.id);
  const userIds: string[] | undefined = userFriends?.map(user => user.id);
  if (userIds !== undefined) {
    const onlineUsers = filterOnlineUsers(usersMap, userIds);
    if (onlineUsers.length > 0) socket.to(onlineUsers).emit(event_message, { user: currentUser });
  }
};

const filterOnlineUsersForNewlyConnectedUsers = async (
  usersMap: UsersMap,
  usersFriendsIdList: string[] | undefined,
  userService: UserService,
): Promise<User[] | null> => {
  const onlineUsersArray: User[] = [];
  const onlineUsers = usersFriendsIdList?.filter(userId => usersMap.has(userId));
  const promises = onlineUsers?.map(async userId => {
    const user: User | null = await userService.findUserById(userId);
    if (user) {
      onlineUsersArray.push(user);
    }
    return userId;
  });
  if (promises) await Promise.all(promises);
  return onlineUsersArray;
};

const sendOnlineUsersToNewUser = async (socket: AuthenticatedSocket, usersMap: UsersMap, event_message: string, userService: UserService) => {
  const userFriends: UserWithStatusAndRoles[] | null = await userService.findUsersFriendsByUserId(socket.user.id);
  const userIds: string[] | undefined = userFriends?.map(user => user.id);
  if (userIds !== undefined) {
    const onlineUsers = await filterOnlineUsersForNewlyConnectedUsers(usersMap, userIds, userService);
    socket.emit(event_message, { users: onlineUsers });
  }
};

const sendOnlineUsersToUserOnRelationChange = async (
  socket: AuthenticatedSocket,
  userId: string,
  usersMap: UsersMap,
  event_message: string,
  userService: UserService,
) => {
  const currentUserSocketIds: string[] = findUserSockets(socket.user.id, usersMap);
  const otherUserSocketIds: string[] = findUserSockets(userId, usersMap);
  const currentUserFriends: UserWithRoles[] | null = await userService.findUsersFriendsByUserId(socket.user.id);
  const otherUserFriends: UserWithRoles[] | null = await userService.findUsersFriendsByUserId(userId);
  let currentUserFriendIds: string[] | undefined = currentUserFriends?.map(user => user.id);
  if (event_message === EVENT_MESSAGES.removedFriend || event_message === EVENT_MESSAGES.blockedFriend) {
    currentUserFriendIds = currentUserSocketIds?.filter(id => {
      return id !== userId;
    });
  }

  const currentUsersOnlineUsers = await filterOnlineUsersForNewlyConnectedUsers(usersMap, currentUserFriendIds, userService);
  if (currentUserSocketIds !== null) socket.to(currentUserSocketIds).emit(event_message, { users: currentUsersOnlineUsers });
  socket.emit(event_message, { users: currentUsersOnlineUsers });
  let otherUserFriendIds: string[] | undefined = otherUserFriends?.map(user => user.id);
  if (event_message === EVENT_MESSAGES.removedFriend || event_message === EVENT_MESSAGES.blockedFriend) {
    otherUserFriendIds = otherUserFriendIds?.filter(id => {
      return id !== socket.user.id;
    });
  }
  const otherUsersOnlineUsers: User[] | null = await filterOnlineUsersForNewlyConnectedUsers(usersMap, otherUserFriendIds, userService);
  if (otherUserSocketIds !== null) socket.to(otherUserSocketIds).emit(event_message, { users: otherUsersOnlineUsers });
};

const findUserSockets = (userId: string, usersMap: UsersMap): string[] => {
  const userSocketMap = usersMap.get(userId);
  const userSocketArray = userSocketMap?.values() || [];
  const sockets = Array.from(userSocketArray);
  return sockets;
};

const findUsersFriends = async (userId: string, prisma: PrismaClient) => {
  const users: UserWithRoles[] | null = await prisma.user.findMany({
    select: {
      id: true,
      name: true,
      surname: true,
      email: true,
      roles: {
        select: {
          role: true,
        },
      },
    },
    where: {
      OR: [
        {
          friendList1: {
            some: {
              OR: [{ userId1: userId }, { userId2: userId }],
              status: {
                type: 1,
              },
            },
          },
        },
        {
          friendList2: {
            some: {
              OR: [{ userId1: userId }, { userId2: userId }],
              status: {
                type: 1,
              },
            },
          },
        },
      ],
    },
  });
  return users;
};
