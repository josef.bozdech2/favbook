import { User } from './UserWithStatusType';

export interface Post {
  id: string;
  content: string;
  isAnnouncement: boolean;
  likesCount: number;
  likes: User[];
  timestamp: Date;
  userId: string;
}

export interface PostWithUser extends Post {
  user: User;
}
