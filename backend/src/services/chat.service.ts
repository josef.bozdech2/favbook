import { prisma, PrismaClient } from '.prisma/client';

export interface Message {
  content: string;
  idUser: string;
  idChat: string;
}
class ChatService {
  public messages = new PrismaClient().message;

  public async getMessagesByUserIdAndFriendId(userId: string, friendId: string, offset: number) {
    const messageList = await this.messages.findMany({
      skip: offset,
      take: 10,
      select: {
        userId: true,
        timestamp: true,
        content: true,
        chatId: true,
        user: {
          select: {
            id: true,
            name: true,
            surname: true,
          },
        },
      },
      where: {
        OR: [
          { AND: [{ chat: { userId1: userId } }, { chat: { userId2: friendId } }] },
          { AND: [{ chat: { userId1: friendId } }, { chat: { userId2: userId } }] },
        ],
      },
      orderBy: {
        timestamp: 'asc',
      },
    });
    return messageList;
  }

  public async postMessageToDatabase(userId: string, message: Message) {
    const newMessage = await this.messages.create({
      data: {
        content: message.content,
        userId: userId,
        chatId: message.idChat,
      },
    });
    return newMessage;
  }
}

export default ChatService;
