import { prisma, PrismaClient } from '.prisma/client';
import { Relation } from '@/types/response/UserWithStatusType';
export interface Message {
  content: string;
  idUser: string;
  idChat: string;
}

const INCOMING_RELATION_ENUM = ['removeFriend', 'addFriend', 'declineFriend', 'requestedFriendship', 'blockUser', 'removeBlockUser'];
class RelationsService {
  public prisma = new PrismaClient();
  public async getFriendshipIdByUserIdAndFriendId(userId: string, friendId: string): Promise<Relation | null> {
    const relation: Relation | null = await this.prisma.friendlist.findFirst({
      select: {
        id: true,
        userId1: true,
        userId2: true,
        status: true,
      },
      where: {
        OR: [{ AND: [{ userId1: userId }, { userId2: friendId }] }, { AND: [{ userId1: friendId }, { userId2: userId }] }],
        status: {
          typeString: 'friends',
        },
      },
    });
    return relation;
  }

  public async createRelation(userId1: string, userId2: string, relationType: number) {
    const relationTypes = await this.prisma.status.findFirst({
      select: {
        id: true,
      },
      where: {
        type: relationType,
      },
    });
    const newRelation = await this.prisma.friendlist.create({
      data: {
        User1: {
          connect: {
            id: userId1,
          },
        },
        User2: {
          connect: {
            id: userId2,
          },
        },
        status: {
          connect: {
            id: relationTypes?.id,
          },
        },
      },
    });
    return newRelation;
  }

  public async updateRelation(userId: string, friendId: string, relationType: number): Promise<void> {
    const currentRelation = await this.prisma.friendlist.findMany({
      select: {
        id: true,
        userId1: true,
        userId2: true,
        status: true,
      },
      where: {
        OR: [{ AND: [{ userId1: userId }, { userId2: friendId }] }, { AND: [{ userId1: friendId }, { userId2: userId }] }],
      },
    });
    if (currentRelation) {
      await currentRelation.forEach(relation => this.deleteRelation(relation.id));
    }
    //add friend request
    if (relationType === 1) {
      await this.createRelation(userId, friendId, 2);
      await this.createRelation(friendId, userId, 3);
    }
    //decline friend
    if (relationType === 2) {
      return;
    }
    //accept request
    if (relationType === 3) {
      await this.createRelation(userId, friendId, 1);
    }
    //decline request
    if (relationType === 4) {
      return;
    }
    //block user
    if (relationType === 5) {
      await this.createRelation(userId, friendId, 4);
      await this.createRelation(friendId, userId, 5);
    }
    //remove block
    if (relationType === 6) {
      return;
    }
  }

  public async deleteRelation(relationId: string) {
    const deletedMessages = await this.prisma.message.deleteMany({
      where: {
        chatId: relationId,
      },
    });
    const removedRelation = await this.prisma.friendlist.delete({
      where: {
        id: relationId,
      },
    });
    return removedRelation;
  }

  public async findRelationsByUserId(userId: string): Promise<Relation[] | null> {
    const relations: Relation[] | null = await this.prisma.friendlist.findMany({
      select: {
        id: true,
        userId1: true,
        userId2: true,
        status: true,
      },
      where: {
        OR: [{ userId1: userId }, { userId2: userId }],
        NOT: [
          {
            status: {
              type: 1,
            },
          },
          {
            status: {
              type: 3,
            },
          },
          {
            status: {
              type: 5,
            },
          },
        ],
      },
    });
    return relations;
  }
}

export default RelationsService;
