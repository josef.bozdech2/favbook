import bcrypt from 'bcrypt';
import config from 'config';
import jwt from 'jsonwebtoken';
import { PrismaClient } from '@prisma/client';
import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken, TokenData } from '@interfaces/auth.interface';
import { isEmpty } from '@utils/util';
import { injectable, singleton } from 'tsyringe';
import { Role, User, UserWithPassword } from '@/types/response/UserWithStatusType';

type UserWithRolesAndPassword = {
  id: string;
  email: string;
  password: string;
  name: string;
  surname: string;
  roles: Role[];
};
type nUserWithRolesAndPassword = UserWithRolesAndPassword | null;
type nUser = User | null;
@injectable()
@singleton()
class AuthService {
  public users = new PrismaClient().user;

  public async signup(userData: CreateUserDto): Promise<{ cookie: string; createUserData: User; token: string }> {
    if (isEmpty(userData)) throw new HttpException(400, 'Email, name, surname or password missing');
    if (userData.email === '' || userData.name === '' || userData.surname === '' || userData.password === '')
      throw new HttpException(400, 'Email, name, surname or password missing');
    const findUser: nUser = await this.users.findUnique({ where: { email: userData.email } });
    if (findUser) throw new HttpException(409, `Email ${userData.email} already exists`);

    const hashedPassword = await bcrypt.hash(userData.password, 10);
    const createUserData: User = await this.users.create({
      select: {
        id: true,
        name: true,
        surname: true,
        email: true,
      },
      data: {
        ...userData,
        password: hashedPassword,
        roles: {
          connect: {
            role: 'USER',
          },
        },
      },
    });

    const tokenData = this.createToken(createUserData);
    const cookie = this.createCookie(tokenData);

    return { cookie, createUserData, token: tokenData.token };
  }

  public async login(userData: CreateUserDto): Promise<{ cookie: string; findUser: nUserWithRolesAndPassword; token: string }> {
    if (isEmpty(userData)) throw new HttpException(400, 'Email or password is missing');
    if (userData.email === '' || userData.password === '') throw new HttpException(400, 'Email or password missing!');
    const findUser: nUserWithRolesAndPassword = await this.users.findUnique({
      where: { email: userData.email },
      select: {
        id: true,
        email: true,
        name: true,
        surname: true,
        password: true,
        roles: {
          select: {
            role: true,
          },
        },
      },
    });
    if (!findUser) throw new HttpException(409, `Email or password is incorrect`);

    const isPasswordMatching: boolean = await bcrypt.compare(userData.password, findUser.password);
    if (!isPasswordMatching) throw new HttpException(409, 'Email or password is incorrect');

    const tokenData = this.createToken(findUser);
    const cookie = this.createCookie(tokenData);

    return { cookie, findUser, token: tokenData.token };
  }

  public async logout(userData: UserWithPassword): Promise<UserWithPassword> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");

    const findUser: UserWithPassword | null = await this.users.findFirst({ where: { email: userData.email, password: userData.password } });
    if (!findUser) throw new HttpException(409, "You're not user");

    return findUser;
  }

  public createToken(user: User | UserWithRolesAndPassword): TokenData {
    const dataStoredInToken: DataStoredInToken = { id: user.id };
    const secretKey: string = config.get('secretKey');
    const expiresIn = 60 * 5;

    return { expiresIn, token: jwt.sign(dataStoredInToken, secretKey, { expiresIn }) };
  }

  public createCookie(tokenData: TokenData): string {
    return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn};`;
  }
}

export default AuthService;
