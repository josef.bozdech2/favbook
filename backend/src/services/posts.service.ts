import { Post, PostWithUser } from '@/types/response/PostType';
import { User } from '@/types/response/UserWithStatusType';
import { PrismaClient } from '@prisma/client';
export interface QueriedPost {
  id: string;
  content: string;
  likesCount: number;
  likes: User[];
  isAnnouncement: boolean;
  timestamp: Date;
  user: PostUser;
}

export interface PostUser {
  id: string;
  name: string;
  surname: string;
}
class PostsService {
  public posts = new PrismaClient().post;
  public async findPostsByUserFriends(userId: string): Promise<PostWithUser[]> {
    const posts: PostWithUser[] = await this.posts.findMany({
      take: 10,
      orderBy: {
        timestamp: 'desc',
      },
      select: {
        id: true,
        content: true,
        likesCount: true,
        likes: true,
        isAnnouncement: true,
        timestamp: true,
        userId: true,
        user: {
          select: {
            id: true,
            name: true,
            surname: true,
            email: true,
          },
        },
      },
      where: {
        OR: [
          {
            user: {
              OR: [
                {
                  friendList1: {
                    some: {
                      OR: [{ userId1: userId }, { userId2: userId }],
                      status: {
                        type: 1,
                      },
                    },
                  },
                },
                {
                  friendList2: {
                    some: {
                      OR: [{ userId1: userId }, { userId2: userId }],
                      status: {
                        type: 1,
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            isAnnouncement: true,
          },
          {
            userId: userId,
          },
        ],
      },
    });
    return posts;
  }

  public async createPost(userId: string, newPost: Post): Promise<Post> {
    const post: Promise<Post> = this.posts.create({
      select: {
        id: true,
        content: true,
        isAnnouncement: true,
        likesCount: true,
        likes: true,
        timestamp: true,
        userId: true,
      },
      data: {
        content: newPost.content,
        isAnnouncement: newPost.isAnnouncement,
        likesCount: 0,
        timestamp: new Date(),
        user: {
          connect: {
            id: userId,
          },
        },
      },
    });
    return post;
  }

  public async changePostLikeStatus(userId: string, postId: string, isLike: boolean): Promise<Post | null> {
    if (isLike) {
      const updatedPost: Post | null = await this.posts.update({
        select: {
          id: true,
          content: true,
          isAnnouncement: true,
          likesCount: true,
          likes: true,
          timestamp: true,
          userId: true,
        },
        where: {
          id: postId,
        },
        data: {
          likes: {
            connect: {
              id: userId,
            },
          },
          likesCount: {
            increment: 1,
          },
        },
      });
      return updatedPost;
    }
    const updatedPost: Post | null = await this.posts.update({
      select: {
        id: true,
        content: true,
        isAnnouncement: true,
        likesCount: true,
        likes: true,
        timestamp: true,
        userId: true,
      },
      where: {
        id: postId,
      },
      data: {
        likes: {
          disconnect: {
            id: userId,
          },
        },
        likesCount: {
          decrement: 1,
        },
      },
    });
    return updatedPost;
  }
}

export default PostsService;
