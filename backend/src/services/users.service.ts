import { Relation, Role, User, UserWithRoles, UserWithRolesAndRelation, UserWithStatusAndRoles } from '@/types/response/UserWithStatusType';
import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { PrismaClient } from '@prisma/client';
import { isEmpty } from '@utils/util';
import bcrypt from 'bcrypt';
import RelationsService from './relations.service';
enum existingRoles {
  user = 'USER',
  admin = 'ADMIN',
  god = 'GOD',
}
class UserService {
  public users = new PrismaClient().user;
  public roles = new PrismaClient().role;
  public relationsService = new RelationsService();

  public async findUserById(userId: string): Promise<User | null> {
    if (isEmpty(userId)) throw new HttpException(400, "You're not userId");

    const findUser: User | null = await this.users.findUnique({
      select: { id: true, name: true, surname: true, email: true },
      where: { id: userId },
    });
    if (!findUser) throw new HttpException(409, "You're not user");

    return findUser;
  }

  public async createUser(userData: CreateUserDto): Promise<User | null> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");

    const findUser: User | null = await this.users.findUnique({ where: { email: userData.email } });
    if (findUser) throw new HttpException(409, `Your email ${userData.email} already exists`);

    const hashedPassword = await bcrypt.hash(userData.password, 10);
    const createUserData: User = await this.users.create({ data: { ...userData, password: hashedPassword, roles: { connect: { role: 'USER' } } } });
    return createUserData;
  }

  public async findUsersBySearchString(userId: string, searchString: string): Promise<UserWithRolesAndRelation[] | null> {
    if (searchString.length < 3) throw new HttpException(409, 'Search string too short!');
    const searchStringSplit: string[] = searchString.split(' ');
    if (searchStringSplit[searchStringSplit.length - 1] === '') {
      searchStringSplit.pop();
    }
    const searchObject: Object[] = [];
    searchStringSplit.forEach(string => {
      searchObject.push({ name: { contains: string } });
      searchObject.push({ surname: { contains: string } });
    });
    const currentUserRelations: Relation[] | null = await this.relationsService.findRelationsByUserId(userId);
    const nonRelatedUsers: UserWithRoles[] | null = await this.users.findMany({
      select: {
        id: true,
        email: true,
        name: true,
        surname: true,
        roles: {
          select: { role: true },
        },
      },
      where: {
        NOT: [
          {
            id: userId,
          },
          {
            friendList1: {
              some: {
                NOT: [{ userId1: userId }],
                userId2: userId,
                status: {
                  OR: [{ type: 5 }, { type: 1 }, { type: 4 }],
                },
              },
            },
          },
          {
            friendList2: {
              some: {
                NOT: [{ userId2: userId }],
                userId1: userId,
                status: {
                  OR: [{ type: 5 }, { type: 1 }, { type: 4 }, { type: 3 }],
                },
              },
            },
          },
        ],
        OR: [
          ...searchObject,
          // {
          //   surname: { contains: 'searchString' },
          // },
        ],
      },
    });
    //merge relations into users
    const nonRelatedUsersWithRelations: UserWithRolesAndRelation[] = nonRelatedUsers?.map(nonRelatedUser => {
      //user has id
      //find user with same id in currentUserRelation to merge with the current user
      const foundRelation = currentUserRelations?.find(relation => relation.userId1 === nonRelatedUser.id || relation.userId2 === nonRelatedUser.id);
      if (foundRelation?.status) return { ...nonRelatedUser, status: foundRelation?.status.type };
      return { ...nonRelatedUser, status: 0 };
    });

    return nonRelatedUsersWithRelations;
  }

  public async findUsersAskingForFriendshipByUserId(userId: string): Promise<UserWithStatusAndRoles[] | null> {
    const users: UserWithRoles[] | null = await this.users.findMany({
      select: {
        id: true,
        email: true,
        name: true,
        surname: true,
        roles: {
          select: { role: true },
        },
      },
      where: {
        friendList2: {
          some: {
            NOT: [{ userId2: userId }],
            userId1: userId,
            status: {
              type: 3,
            },
          },
        },
      },
    });
    const alteredUsers: UserWithStatusAndRoles[] | null = users?.map(user => {
      return {
        ...user,
        status: 3,
      };
    });
    return alteredUsers;
  }

  public async findUsersBlockedByUserId(userId: string): Promise<UserWithStatusAndRoles[] | null> {
    const users: UserWithRoles[] | null = await this.users.findMany({
      select: {
        id: true,
        email: true,
        name: true,
        surname: true,
        roles: {
          select: { role: true },
        },
      },
      where: {
        friendList2: {
          some: {
            NOT: [{ userId2: userId }],
            userId1: userId,
            status: {
              type: 4,
            },
          },
        },
      },
    });
    const alteredUsers: UserWithStatusAndRoles[] | null = users?.map(user => {
      return {
        ...user,
        status: 4,
      };
    });
    return alteredUsers;
  }

  public async findUsersFriendsByUserId(userId: string): Promise<UserWithStatusAndRoles[] | null> {
    const users: UserWithRoles[] | null = await this.users.findMany({
      select: {
        id: true,
        email: true,
        name: true,
        surname: true,
        roles: {
          select: { role: true },
        },
      },
      where: {
        OR: [
          { friendList1: { some: { NOT: [{ userId1: userId }], userId2: userId, status: { type: 1 } } } },
          { friendList2: { some: { NOT: [{ userId2: userId }], userId1: userId, status: { type: 1 } } } },
        ],
      },
    });
    const alteredUsers: UserWithStatusAndRoles[] | null = users?.map(user => {
      return {
        ...user,
        status: 1,
      };
    });
    return alteredUsers;
  }

  public async changeUserRole(userId: string, role: string, mode: string): Promise<UserWithRoles | null> {
    let user: UserWithRoles | null = null;
    if (mode === 'add') {
      user = await this.users.update({
        select: {
          id: true,
          roles: {
            select: {
              role: true,
            },
          },
          name: true,
          surname: true,
          email: true,
        },
        where: {
          id: userId,
        },
        data: {
          roles: {
            connect: {
              role: role,
            },
          },
        },
      });
    }
    if (mode === 'remove') {
      user = await this.users.update({
        select: {
          id: true,
          roles: {
            select: {
              role: true,
            },
          },
          name: true,
          surname: true,
          email: true,
        },
        where: {
          id: userId,
        },
        data: {
          roles: {
            disconnect: {
              role: role,
            },
          },
        },
      });
    }
    return user;
  }

  public async isUserAdmin(userId: string): Promise<boolean> {
    const user: UserWithRoles | null = await this.users.findUnique({
      select: {
        id: true,
        name: true,
        surname: true,
        email: true,
        roles: true,
      },
      where: {
        id: userId,
      },
    });
    if (user?.roles !== undefined) return this.isRole(user?.roles, existingRoles.admin);
    return false;
  }

  public isRole(roles: Role[] | null, requestedRole: string) {
    if (roles === null) return false;
    const filteredRoles = roles.filter(role => {
      return role.role === requestedRole;
    });
    return filteredRoles.length > 0;
  }

  public async updateAdminRole(userId: string, isAddRole: boolean): Promise<User | null> {
    if (isAddRole) {
      const updatedUser: User | null = await this.users.update({
        where: {
          id: userId,
        },
        data: {
          roles: {
            connect: {
              role: existingRoles.admin,
            },
          },
        },
      });
      return updatedUser;
    } else {
      const updatedUser: User | null = await this.users.update({
        where: {
          id: userId,
        },
        data: {
          roles: {
            disconnect: {
              role: existingRoles.admin,
            },
          },
        },
      });
      return updatedUser;
    }
  }
}

export default UserService;
