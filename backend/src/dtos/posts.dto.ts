import { IsBoolean, IsDate, IsInt, IsString } from 'class-validator';

export class CreatePostDto {
  @IsString()
  public content!: string;

  @IsInt()
  public likes!: number;

  @IsDate()
  public timestamp!: number;

  @IsBoolean()
  public isAnnouncement!: boolean;
}
