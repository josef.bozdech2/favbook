import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { RequestHandler } from 'express';
import { HttpException } from '@exceptions/HttpException';

const validationMiddleware = (
  type: any,
  value: string | 'body' | 'query' | 'params' = 'body',
  skipMissingProperties = false,
  whitelist = true,
  forbidNonWhitelisted = true,
): RequestHandler => {
  return (req, res, next) => {
    validate(plainToClass(type, req.body), { skipMissingProperties, whitelist, forbidNonWhitelisted }).then(errors => {
      if (errors.length > 0) {
        next(new HttpException(400, 'Params in form are missing or email is wrong format'));
      } else {
        next();
      }
    });
  };
};

export default validationMiddleware;
