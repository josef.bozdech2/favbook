import { NextFunction, Response } from 'express';
import config from 'config';
import jwt from 'jsonwebtoken';
import { PrismaClient } from '@prisma/client';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken } from '@interfaces/auth.interface';

const users = new PrismaClient().user;
const authMiddleware = async (req: Request | any, res: Response, next: NextFunction) => {
  try {
    const Authorization = req.cookies['Authorization'] || String(req.header('Authorization')).split('Bearer ')[1] || null;

    if (Authorization) {
      const secretKey: string = config.get('secretKey');
      const verificationResponse = (await jwt.verify(Authorization, secretKey)) as DataStoredInToken;
      const userId = verificationResponse.id;

      const findUser = await users.findUnique({ where: { id: String(userId) } });
      if (findUser) {
        req.user = findUser;
        next();
      } else {
        next(new HttpException(401, 'Wrong authentication token'));
      }
    } else {
      next(new HttpException(401, 'Authentication token missing'));
    }
  } catch (error) {
    next(new HttpException(401, 'Wrong authentication token'));
  }
};

export default authMiddleware;
