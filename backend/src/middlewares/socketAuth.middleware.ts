import { NextFunction, Response } from 'express';
import config from 'config';
import jwt from 'jsonwebtoken';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken } from '@interfaces/auth.interface';
import { AuthenticatedSocket } from '@/interfaces/socketAuth.interface';
import { PrismaClient } from '@prisma/client';
import { User } from '@/types/response/UserWithStatusType';
import { ExtendedError } from 'socket.io/dist/namespace';

const users = new PrismaClient().user;
const socketAuthMiddleware = async (socket: AuthenticatedSocket | any) => {
  try {
    const Authorization = socket.handshake.auth.token;

    if (Authorization) {
      const secretKey: string = config.get('secretKey');
      const verificationResponse = (await jwt.verify(Authorization, secretKey)) as DataStoredInToken;
      const userId = verificationResponse.id;

      const findUser: User | null = await users.findUnique({
        select: { id: true, email: true, name: true, surname: true },
        where: { id: String(userId) },
      });
      if (findUser) {
        socket.user = findUser;
      } else {
        new HttpException(401, 'Wrong authentication token');
      }
    } else {
      new HttpException(404, 'Authentication token missing');
    }
  } catch (error) {
    new HttpException(401, 'Wrong authentication token');
  }
};

export default socketAuthMiddleware;
