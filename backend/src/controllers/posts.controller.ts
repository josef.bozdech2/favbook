import PostsService from '@/services/posts.service';
import { NextFunction, Response, Request } from 'express';
import { injectable, singleton } from 'tsyringe';
import express from 'express';
import { RestController } from '@/interfaces/controller.interface';
import authMiddleware from '@/middlewares/auth.middleware';
import { Post, PostWithUser } from '@/types/response/PostType';

@injectable()
@singleton()
class PostsController implements RestController {
  public postsService;
  public path = '/posts';
  constructor(postsService: PostsService) {
    this.postsService = postsService;
  }
  public bindRoutes(router: express.Router) {
    router.use(authMiddleware);
    router.get(`${this.path}`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const postList: PostWithUser[] = await this.postsService.findPostsByUserFriends(String(req.user.id));
        res.status(200).json({ postList, message: 'findPostsByUserId' });
      } catch (error) {
        next(error);
      }
    });
    router.post(`${this.path}`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        if (req.body.post.content === '') {
          res.status(400).json({ message: 'Cannot create empty post' });
        } else {
          const newPost = await this.postsService.createPost(req.user.id, req.body.post);
          res.status(200).json({ newPost, message: 'new post created' });
        }
      } catch (error) {
        next(error);
      }
    });
    router.post(`${this.path}/likes`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const updatedPost: Post | null = await this.postsService.changePostLikeStatus(String(req.user.id), req.body.postId, req.body.setAsLike);
        res.status(200).json({ updatedPost, message: 'post liked' });
      } catch (error) {
        next(error);
      }
    });
  }
}

export default PostsController;
