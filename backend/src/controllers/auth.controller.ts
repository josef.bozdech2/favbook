import { RestController } from '@/interfaces/controller.interface';
import authMiddleware from '@/middlewares/auth.middleware';
import validationMiddleware from '@/middlewares/validation.middleware';
import { UserWithPassword } from '@/types/response/UserWithStatusType';
import { CreateUserDto } from '@dtos/users.dto';
import AuthService from '@services/auth.service';
import express, { NextFunction, Request, Response } from 'express';
import { autoInjectable, singleton } from 'tsyringe';
@autoInjectable()
@singleton()
class AuthController implements RestController {
  public authService;
  constructor(authService: AuthService) {
    this.authService = authService;
  }
  public bindRoutes(router: express.Router) {
    router.post(
      `/signup`,
      validationMiddleware(CreateUserDto, 'body', true),
      async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
          const userData: CreateUserDto = req.body;
          const { cookie, createUserData, token } = await this.authService.signup(userData);

          res.setHeader('Set-Cookie', [cookie]);
          res.status(200).json({ data: createUserData, message: 'login', token });
        } catch (error) {
          next(error);
        }
      },
    );
    router.post(
      `/login`,
      validationMiddleware(CreateUserDto, 'body', true),
      async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
          const userData: CreateUserDto = req.body;
          const { cookie, findUser, token } = await this.authService.login(userData);

          res.setHeader('Set-Cookie', [cookie]);
          res.status(200).json({ data: findUser, message: 'login', token });
        } catch (error) {
          next(error);
        }
      },
    );
    router.post(`/logout`, authMiddleware, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const userData: UserWithPassword | undefined = req.user;
        if (userData) {
          const logOutUserData: UserWithPassword = await this.authService.logout(userData);

          res.setHeader('Set-Cookie', ['Authorization=; Max-age=0']);
          res.status(200).json({ data: logOutUserData, message: 'logout' });
        }
      } catch (error) {
        next(error);
      }
    });
  }
}

export default AuthController;
