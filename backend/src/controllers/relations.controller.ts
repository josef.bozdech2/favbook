import { RestController } from '@/interfaces/controller.interface';
import authMiddleware from '@/middlewares/auth.middleware';
import RelationsService from '@/services/relations.service';
import { NextFunction, Response, Router, Request } from 'express';
import { injectable, singleton } from 'tsyringe';

@injectable()
@singleton()
class RelationsController implements RestController {
  public relationsService;
  public path = '/relations';
  constructor(relationsService: RelationsService) {
    this.relationsService = relationsService;
  }
  public bindRoutes(router: Router) {
    router.use(authMiddleware);
    router.post(`${this.path}/update`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        await this.relationsService.updateRelation(req.user.id, req.body.friendId, req.body.relationType);
        res.status(201).json({ message: 'Relation successfuly updated!' });
      } catch (error) {
        next(error);
      }
    });
  }
  public postRelation = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const newRelation = await this.relationsService.createRelation(req.body.id, req.body.friendId, req.body.relationType);
      res.status(200).json({ newRelation, message: 'new relation created' });
    } catch (error) {
      next(error);
    }
  };
}

export default RelationsController;
