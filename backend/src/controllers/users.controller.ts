import { HttpException } from '@/exceptions/HttpException';
import { CreateUserDto } from '@dtos/users.dto';
import { NextFunction, Response, Router, Request } from 'express';
import { injectable, singleton } from 'tsyringe';
import UserService from '@services/users.service';
import { RestController } from '@/interfaces/controller.interface';
import authMiddleware from '@/middlewares/auth.middleware';
import { FindSearchedUsersResponse, User, UserWithRolesAndRelation, UserWithStatus } from '@/types/response/UserWithStatusType';
@injectable()
@singleton()
class UsersController implements RestController {
  public userService;
  public path = '/users';
  constructor(userService: UserService) {
    this.userService = userService;
  }
  public bindRoutes(router: Router) {
    router.use(authMiddleware);
    router.get(`${this.path}/search`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const userId = String(req.user.id);
        const foundUsers: UserWithRolesAndRelation[] | null = await this.userService.findUsersBySearchString(userId, String(req.query.searchString));
        const dtoOut: FindSearchedUsersResponse = { userList: foundUsers, message: 'foundUsers' };
        res.status(200).json(dtoOut);
      } catch (error) {
        next(error);
      }
    });
    router.get(`${this.path}/relations`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const userId = String(req.query.id);
        const friendshipRequestingUsers: UserWithStatus[] | null = await this.userService.findUsersAskingForFriendshipByUserId(userId);
        const currentFriends: UserWithStatus[] | null = await this.userService.findUsersFriendsByUserId(userId);
        const blockedUsers: UserWithStatus[] | null = await this.userService.findUsersBlockedByUserId(userId);
        res.status(200).json({ data: { friendshipRequestingUsers, currentFriends, blockedUsers }, message: 'friendshipRequests' });
      } catch (error) {
        next(error);
      }
    });
    router.get(`${this.path}/friendships`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const userId = String(req.user.id);
        const friendshipRequestingUsers: UserWithStatus[] | null = await this.userService.findUsersAskingForFriendshipByUserId(userId);
        res.status(200).json({ data: friendshipRequestingUsers, message: 'friendshipRequests' });
      } catch (error) {
        next(error);
      }
    });
    router.post(`${this.path}/admin`, async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const userId = String(req.user.id);
        const isPermitted: boolean = await this.userService.isUserAdmin(userId);
        if (!isPermitted) throw new HttpException(403, 'Forbidden');
        const updatedUser: User | null = await this.userService.updateAdminRole(req.body.userId, req.body.isAddRole);
        res.status(201).json({ data: { updatedUser, messsage: 'userUpdated' } });
      } catch (error) {
        next(error);
      }
    });
  }
}

export default UsersController;
