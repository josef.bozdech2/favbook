import express from 'express';
export interface RestController {
  bindRoutes: (router: express.Router) => void;
}
