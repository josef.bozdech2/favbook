import { User } from '@/types/response/UserWithStatusType';
import { Socket } from 'socket.io';

export interface AuthenticatedSocket extends Socket {
  user: User;
}
