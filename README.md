Favn’t - PIA semester project

Usage
1) Download project from https://gitlab.com/josef.bozdech2/favbook
2) In the root folder run docker-compose up, during the startup, server may restart once, that’s because of migration script which creates the tables in the databe for the project to function properly
3) Go get a coffee
4) Go to localhost:3000 to find the home page

Solution

Technologies
1) Frontend - React
2) Backend - Node.js
3) Database - MySQL

I have implemented my website with 4 separate screens - Login, Sign up, Home and User search.
You can access each screen via router (or links, that’s between Login and Sign up screens).
Only authorized user can access Home and User search screens. To access these screens you can 
either create a new account or use already existing one.

I use REST calls to get all data required to display on the screens. There is a 5 second update 
time between calls. You can like other users posts via the like button, then show liking users by clicking on the 
number below the post.

You can visit User search screen by clicking the search button on the NavBar. Atleast 3 characters are required to 
search users. Search will show all the users containing parts of the string divided by space. To go back to 
Home page, click favn’t text on NavBar. 

To logout, use logout button on NavBar.

To communicate with other online users, you have to click their name on Home page, then write a message and click 
send icon on the bottom left of the screen.

Bonus parts
1) Storing of chat messages
2) Instant check of email availability
3) Post likes
4) Swagger (accessible in the /backend folder or at http://localhost:5000/api-docs)
5) React frontend technology
